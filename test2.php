<?php
$password = "tungdeptrai";
$messageClear = "Yeu em ta yeu tuoi tho ngay - lao dong hang say tinh yeu se den";

// 32 byte binary blob
$aes256Key = substr(hash("SHA256", $password),0,32);

// for good entropy (for MCRYPT_RAND)
srand((double) microtime() * 1000000);
// generate random iv
//$iv = mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_CBC), MCRYPT_RAND);
$iv = substr(hash("SHA256","laodonghangsay"),0,32);

$crypted = fnEncrypt($messageClear, $aes256Key);

//$newClear = fnDecrypt($crypted, $aes256Key);

echo
"<code>".$iv."</code><br/>".
"<code>".$aes256Key."</code><br/>".
"<code>".$crypted."</code><br/>";

function fnEncrypt($sValue, $sSecretKey) {
    global $iv;
    return rtrim(base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $sSecretKey, $sValue, MCRYPT_MODE_CBC, $iv)), "\0\3");
}

function fnDecrypt($sValue, $sSecretKey) {
    global $iv;
    return rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $sSecretKey, base64_decode($sValue), MCRYPT_MODE_CBC, $iv), "\0\3");
}

function strToHex($string){
	//return $string;
    $hex = '';
    for ($i=0; $i<strlen($string); $i++){
        $ord = ord($string[$i]);
        $hexCode = dechex($ord);
        $hex .= substr('0'.$hexCode, -2);
    }
    return strToUpper($hex);
}
function hexToStr($hex){
    $string='';
    for ($i=0; $i < strlen($hex)-1; $i+=2){
        $string .= chr(hexdec($hex[$i].$hex[$i+1]));
    }
    return $string;
}
