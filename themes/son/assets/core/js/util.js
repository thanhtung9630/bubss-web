$__$.loadScript = function(src,document,callback){
	if(!document)
		document = window.document;
	var s, r, t;
	  r = false;
	  s = document.createElement('script');
	  s.type = 'text/javascript';
	  s.src = src;
	  s.onload = s.onreadystatechange = function() {
	    //console.log( this.readyState ); //uncomment this line to see which ready states are called.
	    if ( !r && (!this.readyState || this.readyState == 'complete') )
	    {
	      r = true;
	      callback();
	    }
	  };
	  //t = document.getElementsByTagName('meta')[0];
	  //t.parentNode.insertBefore(s, t);
	  t = document.getElementsByTagName("head")[0];
	  t.appendChild(s);
};

$__$.generateRandomString = function(length){
    var d = new Date().getTime();
	function generateUUID(){
	    var uuid = 'xxxxx'.replace(/[xy]/g, function(c) {
	        var r = (d + Math.random()*16)%16 | 0;
	        d = Math.floor(d/16);
	        return (c=='x' ? r : (r&0x7|0x8)).toString(16);
	    });
	    return uuid;
	};
	return generateUUID()+new Date().getTime();
};

$__$.prevent = function(e)
{
	e = e ? e : window.event;
	if(e)
	{
		e.stopPropagation();
		e.preventDefault();
	}
	return false;
}

$__$.on("html-appended",function($html){
	$html.find("[data-toggle='tooltip']:not([tooltip-done])").each(function(){
		$(this).attr("tooltip-done","").tooltip();
	});
	$html.find("[data-toggle='popover']:not([popover-done])").each(function(){
		$(this).attr("popover-done","").popover();
	});
});

$__$.toUnderscore = function(str){
	return str.replace(/([A-Z])/g, function($1){return "_"+$1.toLowerCase();});
};

$.fn.reset = function(){
	$(this).get(0).reset();
	$(this).find("input,select").trigger("change");
}

/*(function(){
	var originalSerializeArray = $.fn.serializeArray;
	$.fn.extend({
	    serializeArray: function () {
	        var brokenSerialization = originalSerializeArray.apply(this);
	        var checkboxValues = $(this).find('input[type=checkbox]:not([checkbox-origin])').map(function () {
	            return { 'name': this.name, 'value': this.checked ? this.value : 0 };
	        }).get();
	        var checkboxKeys = $.map(checkboxValues, function (element) { return element.name; });
	        var withoutCheckboxes = $.grep(brokenSerialization, function (element) {
	            return $.inArray(element.name, checkboxKeys) == -1;
	        });

	        return $.merge(withoutCheckboxes, checkboxValues);
	    }
	});
})();*/

$.fn.bindFirst = function(name, fn) {
    // bind as you normally would
    // don't want to miss out on any jQuery magic
    this.on(name, fn);

    // Thanks to a comment by @Martin, adding support for
    // namespaced events too.
    this.each(function() {
        var handlers = $._data(this, 'events')[name.split('.')[0]];
        console.log(handlers);
        // take out the handler we just inserted from the end
        var handler = handlers.pop();
        // move it at the beginning
        handlers.splice(0, 0, handler);
    });
};

$.fn.removeAllData = function(){
	var data = $(this).data();
	for(var k in data){
		$.removeData($(this),k);
	}
}

$.fn.scrollToMe = function(){
	$('html, body').scrollTop($(this).offset().top);
	return $(this);
};

jQuery.fn.textFocusEnd = function(){
	var el = $(this).get(0);
    if (typeof el.selectionStart == "number") {
        el.selectionStart = el.selectionEnd = el.value.length;
    } else if (typeof el.createTextRange != "undefined") {
        el.focus();
        var range = el.createTextRange();
        range.collapse(false);
        range.select();
    }
    return $(this);
};

jQuery.fn.textFocusAll = function(){
	var el = $(this).get(0);
    if (typeof el.selectionStart == "number") {
        el.selectionStart = 0;
        el.selectionEnd = el.value.length;
    } else if (typeof el.createTextRange != "undefined") {
        el.focus();
        var range = el.createTextRange();
        range.collapse(false);
        range.select();
    }
    return $(this);
};

$__$.registerJQueryPlugin("selectDropdown",function(){
	var $self = this;
	var $elem = $self.$elem;
	var options = $self.options;
	var $options;
	var $html;
	var searchCache = {};

	this.onInit = function(){
		if($__$.mobile && !options.ajax){
			$elem.show();
			return;
		}
		$self.initHtml();
		$self.updateOptions();
		$self.updateValue();
	}

	this.onUpdate = function(){
		if($__$.mobile && !options.ajax){
			$elem.show();
			return;
		}
		$self.updateValue();
	}

	this.initHtml = function(){
		var html = '\
			<div class="btn-group">\
				<button type="button" class="btn ' + options.btnClass + ' dropdown-toggle" data-toggle="dropdown">\
					<span class="select-text"></span>\
					<span class="caret"></span>\
				</button>\
				<ul class="dropdown-menu option-list" role="menu" style="left:auto">\
		';
		if(options.ajax){
			html += '\
				<li class="item-search"> \
					<div class="search-container empty"> \
						<input type="text" class="search form-control" placeholder="Search" /> \
						<div class="loading"></div> \
						<i class="icon-remove close"></i> \
					</div> \
				</li> \
			';
		}
		
		html += '\
				</ul>\
			</div>\
		';
	
		$self.$html && $self.$html.remove();
		$self.$html = $html = $(html).hide().insertAfter($elem);

		// events
		

		$html.children(".dropdown-toggle").click(function(e){
			//$html.dropdown("toggle");
			$html.toggleClass("open");
			if($html.hasClass("open")){
				$html.find(".search").focus();
			}
			return $__$.prevent(e);
		});

		// search

		if(options.ajax){
			$html.find("ul li div.search-container").on("click",function(e){
				return $__$.prevent(e);
			});
			$html.find(".search").listenToEnterKey(null,true).on("enter",function(e){
				$self.search($(this).val());
				return $__$.prevent(e);
			}).on("change keyup",function(){
				if($(this).val()==""){
					$html.find(".search-container").addClass("empty");
					$(this).focus();
				}
				else {
					$html.find(".search-container").removeClass("empty");	
				}
			});

			$html.on('shown.bs.dropdown', function () {
				$html.find("ul li.item-search input").focus();
			});

			$html.find(".close").click(function(){
				$html.find(".search").val("").trigger("change");
			});

			$elem.on("change",function(){
				var val = $(this).data("value");
				if(val===null){
					val = $(this).val();
				}
				var $selectOption = $elem.find("option[value='"+val+"']");
				if(!$selectOption.length){
					// no selected options => refresh
					$elem.children().remove();
					$elem.append('<option value="'+val+'" selected="selected">'+$elem.data("display")+'</option>');
				}
				$self.updateOptions();
				$self.updateValue();
			});

		}



		$elem.on("change",function(){
			$self.updateValue();
		});


		// start

		$elem.hide();
		$html.show();
	}

	this.updateOptions = function(){
		$options = $elem.children("option");
		var optionHtml = "";
		$options.each(function(){
			var optionVal = $(this).val();
			var optionText = $(this).text();
			optionHtml += '<li><a href="javascript:;" data-value="'+optionVal+'" data-text="'+optionText+'">'+optionText+'</a></li>';
		});
		var $optionHtml = $(optionHtml);

		if(options.ajax){
			var $searchItem = $html.find(".item-search");
			$searchItem.nextAll().remove();
			$optionHtml.insertAfter($searchItem);
		} else {
			$html.find(".option-list").html($optionHtml);
		}

		$optionHtml.find("a").on("click",function(e){
			var optionVal = $(this).data("value");
			$elem.val(optionVal).trigger("change");
			$html.removeClass("open");
			return $__$.prevent(e);
		});
	}

	this.updateValue = function(val){
		var val = $elem.val();
		var text = $.trim($options.filter(":selected").text());
		$elem.data("value",val);
		$elem.data("display",text);
		$html.find(".select-text").text(text);
	}

	this.search = function(keyword){
		keyword = $.trim(keyword);
		$self.$elem.val(keyword);
		if(keyword.length==0)
			return;
		var resultCallback = function(results){
			if(options.searchcache){
				searchCache[keyword] = results;
			}
			$elem.children().remove();
			var selectedValue = $self.$elem.data("value");
			var selectedText = $self.$elem.data("display");
			if(selectedValue!=undefined && selectedText!=undefined){
				$elem.prepend('<option value="'+selectedValue+'" selected="selected">'+selectedText+'</option>');
			}
			$.each(results,function(k,v){
				if(v[0]==selectedValue)
					return;
				$elem.append('<option value="'+v[0]+'">'+v[1]+'</option>');
			});
			$self.updateOptions();
			$self.updateValue();
			$html.find(".search-container").removeClass("searching");
		};
		if($self.options.searchcache && searchCache[keyword]!=undefined){
			resultCallback(searchCache[keyword]);
			return;
		}
		$html.find(".search-container").addClass("searching");
		$.ajax({
			type : "get",
			url : options.url,
			data : {
				model : options.model,
				attr : options.attr,
				term : keyword,
			},
			success : function(json){
				$__$.handleJSON(json,function(results){
					resultCallback(results);
				});
			}
		});
	}

},{
	btnClass : "btn-default btn-sm",
	ajax : "",
	model : "",
	url : "",
	searchcache : 1,
	defaultDisplayText : "No change",
},"[input-dropdown]");

// fix top

$__$.registerJQueryPlugin("elemFixedTop",function(){
	var $self = this;
	var $elem = $self.$elem;
	var options = $self.options;

	this.onInit = function(){
		var isOverflow = false;
		var originTop;
		var originWidth;
		var originHeight = $elem.get(0).offsetHeight;
		var minHeight = parseFloat($("body").css("min-height"));

		if(originHeight > minHeight)
			$("body").css("min-height",originHeight);
		$elem.parent().css("position","relative");
		$(window).scroll(function(){
			if(!$elem.is(":visible"))
				return;
			var top = originTop ? originTop : $elem.offset().top - parseFloat($elem.css('margin-top').replace(/auto/, 0)) - $elem.get(0).offsetHeight;
			//console.log(top);
			var overflow = $(window).scrollTop() > top;
			if((isOverflow && overflow) || (!isOverflow && !overflow))
				return;
			console.log("elem-fixed-top change");
			isOverflow = overflow;
			if(overflow)
			{
				if(!originTop)
				{
					originTop = top;
				}
				if(!originWidth)
				{
					originWidth = $elem.get(0).offsetWidth;
					$elem.css("width",originWidth);
				}
				$elem.addClass("elemFixedTopClass").css("top",options.top);
			}
			else
			{
				$elem.removeClass("elemFixedTopClass").css("top","auto");
			}
		});
	}
},{
	top : 0
},"[position-fixed-top]");


$__$.registerJQueryPlugin("heightUntilBottom",function(){
	var $self = this;
	var $elem = $self.$elem;
	var options = $self.options;

	this.onInit = function(){
		var offset = $elem.offset();
		if(!offset || offset.top==undefined)
			return;
		var height = window.innerHeight - offset.top;
		//console.log($(this).offset().top);
		//console.log(height);
		if(isNaN(height))
			return;
		$elem.css("height",height);
		if(options.overflowAuto)
			$elem.css("overflow","auto");
	}
},{
	overflowAuto : true
},"[height-until-bottom]");

(function(){
	var arr = [];
	var humanize = function(timestamp,lang){
		return new Date(timestamp * 1000).toLocaleString();
		//
		var sPerMinute = 60;
		var sPerHour = sPerMinute * 60;
		var sPerDay = sPerHour * 24;
		var sPerMonth = sPerDay * 30;
		var sPerYear = sPerDay * 365;
		var current = (new Date()).getTime()/1000;
		var d = window.d_timestamp ? window.d_timestamp : 0;
		var elapsed = current + d - timestamp; // num second

		switch(lang){
			case "vi":
				if (elapsed < sPerMinute) {
					return "vừa xong";
				}
				else if (elapsed < sPerHour) {
					 return Math.round(elapsed/sPerMinute) + ' phút trước';   
				}
				else if (elapsed < sPerDay ) {
					 return Math.round(elapsed/sPerHour ) + ' giờ trước';   
				}
				/*else if (elapsed < sPerMonth) {
					return 'khoảng ' + Math.round(elapsed/sPerDay) + ' ngày trước';   
				}
				else if (elapsed < sPerYear) {
					return 'khoảng ' + Math.round(elapsed/sPerMonth) + ' tháng trước';   
				}
				else {
					return 'khoảng ' + Math.round(elapsed/sPerYear ) + ' năm trước';   
				}*/
				break;
			default : // en
				if (elapsed < sPerMinute) {
					return "just now";
				}
				else if (elapsed < sPerHour) {
					var val = Math.round(elapsed/sPerMinute);
					return val + ' minute' + (val > 1 ? "s" : "") + " ago";
				}
				else if (elapsed < sPerDay ) {
					var val = Math.round(elapsed/sPerHour);
					return val + ' hour' + (val > 1 ? "s" : "") + " ago";
				}
				/*else if (elapsed < sPerMonth) {
					var val = Math.round(elapsed/sPerDay);
					return val + ' day' + (val > 1 ? "s" : "") + " ago";
				}
				else if (elapsed < sPerYear) {
					var val = Math.round(elapsed/sPerMonth);
					return val + ' month' + (val > 1 ? "s" : "") + " ago";
				}
				else {
					var val = Math.round(elapsed/sPerYear );
					return val + ' year' + (val > 1 ? "s" : "") + " ago"; 
				}*/
				break;
		}

		return new Date(timestamp * 1000).toLocaleString();

		
	};

	$__$.registerJQueryPlugin("prettyTime",function(){
		var $self = this;
		var $elem = $self.$elem;
		var options = $self.options;

		this.onInit = function(){
			$self.update();
		}

		this.onUpdate = function(){
			$self.update();
		}

		this.update = function(){
			var timestamp = $elem.attr("timestamp") ? $elem.attr("timestamp") : $elem.text();
			timestamp = parseInt(timestamp);
			if(!timestamp)
				return;
			setInterval(function(){
				$elem.text(humanize(timestamp,options.lang));
			}, options.interval);
			$elem.text(humanize(timestamp,options.lang));
		}
	},{
		interval : 30000,
		lang : "en"
	},"[timestamp]");
})();

$.fn.realVal = function(setVal){
	var returnValue = $(this);
	$(this).each(function(){
		if($(this).is('[type="checkbox"]')){
			if(setVal==undefined){
				returnValue = $(this).is(":checked") ? 1 : 0;
			} else {
				$(this).prop('checked', parseInt(setVal));
			}
		} else if($(this).is('[type="file"]')){
			if(setVal==undefined){
				returnValue = $(this).attr("data-url");
			} else {
				$(this).attr("data-url",setVal);
			}
		} else {
			if(setVal==undefined){
				returnValue = $(this).val();
			} else {
				if($(this).is("select")){
					$(this).data("value",setVal);
				}
				$(this).val(setVal);
			}
		}
	});
	return returnValue;
};

$__$.registerJQueryPlugin("inputChangeTracker",function(){
	var $self = this;
	var $elem = $self.$elem;
	var options = $self.options;
	var triggerEnabled = true;
	var originValue;
	var currentValue;

	this.onInit = function(){
		originValue = $elem.realVal();
		currentValue = originValue;
		$elem.attr("origin-value",originValue);
		$elem.on("change keyup",function(e){
			var hasChanged = $self.detectChange();
			if(triggerEnabled && hasChanged)
				$elem.trigger("changed");
		});
		$elem.on("change-without-trigger-changed",function(){
			triggerEnabled = false;
			$elem.trigger("change");
			triggerEnabled = true;
		});
		
		$elem.on("changed-without-check",function(){
			$elem.attr("changed","");
		});
	}

	this.recover = function(){
		$elem.val(originValue).trigger("change-without-trigger-changed").removeAttr("changed");
	}

	this.detectChange = function(){
		var newValue = $elem.realVal();
		var valueChanged = newValue!=currentValue;
		var valueHasChangedBackToOrigin = newValue==originValue;
		if(valueHasChangedBackToOrigin){
			if(options.recoverEnabled)
				$elem.removeAttr("changed");
		} else if(valueChanged) {
			$elem.attr("changed","");
		}
		currentValue = newValue;
		return valueChanged;
	}
},{
	recoverEnabled : 1
},"[track-change]");

$__$.registerJQueryPlugin("listenToEnterKey",function(){
	var $self = this;
	var $elem = $self.$elem;
	var options = $self.options;

	this.onInit = function(){
		$elem.keydown(function(e){
		    if(e.keyCode == 13)
		    {
		        $elem.trigger("enter");
		        var enterCallback = $elem.attr("onenter");
		        if(enterCallback)
		        	eval(enterCallback);
		        return $__$.prevent(e);
		    }
		});
	}
},{},"[onenter]");

$__$.registerJQueryPlugin("inlineEdit",function(){
	var $self = this;
	var $elem = $self.$elem;
	var options = $self.options;
	var type;

	this.onInit = function(){
		if(options.inlineEditType=="double_click"){
			var id = $elem.attr("inline-edit-id");
			var $input = $("[inline-edit-display-id='"+id+"']");
			function showInput(){
				$elem.hide();
				$input.show().removeAttr("hidden");
			}

			function hideInput(){
				$elem.show();
				$input.hide();
			}

			$self.recover = function(){
				hideInput();
				$input.getPlugin("inputChangeTracker").recover();
				$elem.trigger("inline-edit-canceled");
			}

			$elem.on("click",function(){
				showInput();
			});

			$input.on("blur",function(){
				$self.recover();
			});
			$input.on("keyup",function(e){
				if(e.keyCode==27)
					$self.recover();
			})

			$self.listenInputChanged($input,function(){

			});
		} else if(options.inlineEditType=="show_input_directly"){
			$self.listenInputChanged($elem,function(){

			});
		}
	}

	// TODO
	this.updateValue = function(){
		if(options.inlineEditType=="double_click"){

		} else {
			
		}
	}

	this.listenInputChanged = function($input,callback){
		function triggerChange(){
			$elem.trigger("inline-edit-changed",{
				value : $input.realVal()
			});
			callback && callback();
		}

		$input.inputChangeTracker(null,true);

		if(options.inlineEditType=="double_click"){
			$input.listenToEnterKey(null,true).on("enter",function(){
				if($input.is("[changed]")){
					triggerChange();
				}
			});
		} else {
			$input.on("changed",function(){
				triggerChange();
			});
		}
	}
},{
	inlineEditType : "show_input_directly"
},"[inline-edit]");