-- phpMyAdmin SQL Dump
-- version 4.4.6.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 16, 2015 at 12:35 PM
-- Server version: 5.6.24
-- PHP Version: 5.5.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `bubss`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_file`
--

CREATE TABLE IF NOT EXISTS `tbl_file` (
  `id` int(11) NOT NULL,
  `active` int(1) NOT NULL DEFAULT '1',
  `created_time` int(11) NOT NULL,
  `updated_time` int(11) NOT NULL,
  `main_file_id` int(11) NOT NULL,
  `member_id` int(11) NOT NULL,
  `file` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_file`
--

INSERT INTO `tbl_file` (`id`, `active`, `created_time`, `updated_time`, `main_file_id`, `member_id`, `file`) VALUES
(24, 1, 1444041048, 1444041048, 25, 71, '/upload/user/71/user manual_1444041048.pdf.bubss'),
(25, 1, 1444041048, 1444041048, 25, 68, '/upload/user/68/user manual_1444041048.pdf.bubss'),
(26, 1, 1444041048, 1444041048, 26, 71, '/upload/user/71/technical manual_1444041049.pdf.bubss'),
(27, 1, 1444041048, 1444041048, 26, 68, '/upload/user/68/technical manual_1444041049.pdf.bubss'),
(28, 1, 1444041048, 1444041048, 27, 71, '/upload/user/71/mo ta he thong doc file usb_1444041049.docx.bubss'),
(29, 1, 1444041048, 1444041048, 27, 68, '/upload/user/68/mo ta he thong doc file usb_1444041049.docx.bubss');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_main_file`
--

CREATE TABLE IF NOT EXISTS `tbl_main_file` (
  `id` int(11) NOT NULL,
  `active` int(1) NOT NULL DEFAULT '1',
  `created_time` int(11) NOT NULL,
  `updated_time` int(11) NOT NULL,
  `admin_id` int(11) NOT NULL,
  `file_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_main_file`
--

INSERT INTO `tbl_main_file` (`id`, `active`, `created_time`, `updated_time`, `admin_id`, `file_name`, `description`) VALUES
(25, 1, 1444041048, 1444041048, 1, 'user manual', 'abc'),
(26, 1, 1444041048, 1444041048, 1, 'technical manual', 'abc'),
(27, 1, 1444041048, 1444041048, 1, 'mo ta he thong doc file usb', 'abc');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE IF NOT EXISTS `tbl_user` (
  `id` int(11) NOT NULL,
  `active` int(1) DEFAULT '1',
  `created_time` int(11) NOT NULL,
  `updated_time` int(11) NOT NULL,
  `name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `random_code` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `admin_id` int(11) NOT NULL,
  `access_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`id`, `active`, `created_time`, `updated_time`, `name`, `email`, `password`, `random_code`, `admin_id`, `access_token`) VALUES
(1, 1, 1443535119, 1444123434, 'Admin A', 'admina@gmail.com', '$2a$13$cag57yCVGkvPH1l6r/dK7OmCx1.mEYnC1rXo09O8mkltRj4tH/rMC', NULL, 0, NULL),
(68, 1, 1443538202, 1444066873, 'User A1', 'usera1@gmail.com', '$2a$13$QS3JST0BKDdoB/WW1OhlZ.vLEGVYxJgy.i0o8gAGUPhrDkUSg7q2y', 'B4HhkEtmq2ByeIb4BDMDRgwsQAZgV4c64c0eaad57a68cab0b8', 1, NULL),
(69, 1, 1443538221, 1443538469, 'User A2', 'usera2@gmail.com', '$2a$13$GGNfqBX.lEucNOZfuu6ZwuCnQFYYe2YuYNCt8v37XAgQNPIrSAVDW', 'rFH47HE1WQ94CrGjmlclUUcftWCDLP109ea856ccc37acfaf2d', 1, NULL),
(70, 1, 1443538233, 1443538233, 'User A3', 'usera3@gmail.com', '$2a$13$TOIFG2oGRQyiXOPnGOixj.M8nbiX8RzVWpdwlHjNFWYIUfPnlrHZm', 'tbxwa6jMW1JDNxusaKQ52ftRS7DCEY5707aa5495a144df3434', 1, NULL),
(71, 1, 1443538245, 1443538245, 'User A4', 'usera4@gmail.com', '$2a$13$6ObFZnX3tFJ083a2yYi02OVitpepOs32W3BEJS0iv9jqtb.S8M.w6', '1IPTRZnuRMLPgWYwLOsEAebOsxD7BBf80643c26fc3ff34e5aa', 1, NULL),
(72, 1, 1444027473, 1444027473, 'Admin C', 'adminc@gmail.com', '$2a$13$LrxKOvfwE/v0wNmGb/pJ6.Ykwsvidwo7Tn79zMKM2DTZrZO9gZ4QG', 'ISkVIHedx0pNuhYSruwcI68Yb5pVaVb3e19007d7f871d0714e', 0, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_file`
--
ALTER TABLE `tbl_file`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_main_file`
--
ALTER TABLE `tbl_main_file`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_file`
--
ALTER TABLE `tbl_file`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `tbl_main_file`
--
ALTER TABLE `tbl_main_file`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=73;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
