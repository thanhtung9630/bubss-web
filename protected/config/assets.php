<?php
return array(
	"son" => array(
		"__autoloadExtensions" => array(
			"bootstrap", "font-awesome", "bootstrap-datepicker", "select2", "icheck"
		),
		"core" => array(
			"js" => array(
				"jquery.js", "core.js", "util.js", "formajax.js"
			),
			"css" => array(
				"style.css",
				"main.css"
			),
		),
		"extensions" => array(
			"bootstrap" => array(
				"css" => array(
					"bootstrap.min.css",
					"bootstrap-theme.min.css"
				),
				"js" => "bootstrap.min.js"
			),
			"font-awesome-3.2.1" => array(
				"css" => "font-awesome.css",
			),
			"font-awesome" => array(
				"css" => "font-awesome.css",
			),
			"bootstrap-datepicker" => array(
				"js" => array(
					"bootstrap-datepicker.js",
					'$__$.js'
				),
				"css" => array(
					"datepicker.css"
				)
			),
			"bootstrap-datetimepicker" => array(
				"js" => array(
					"bootstrap-datetimepicker.js",
					'$__$.js'
				),
				"css" => array(
					"bootstrap-datetimepicker.css"
				),
				"dependency" => array(
					"moment"
				)
			),
			"moment" => array(
				"js" => array(
					"moment.js"
				)
			),
			"bootstrap-fileinput" => array(
				"css" => array(
					"fileinput.css"
				),
				"js" => array(
					"fileinput.js", '$__$.js'
				)
			),
			"upload-preview" => array(
				"css" => "upload-preview.css",
				"js" => "upload-preview.js"
			),
			"select2" => array(
				"css" => array(
					"select2.css" ,"select2-metronic.css"
				), 
				"js" => array(
					"select2.js", '$__$.js'
				)
			),
			"angular" => array(
				"js" => array(
					"angular.min.js",
					"angular-animate.min.js",
					"angular-route.min.js",
					"angular-touch.min.js",
					'$__$.js'
				),
				"angular-ui" => array(
					"js" => "angular-ui.js"
				)
			),
			"summernote" => array(
				"js" => array(
					"summernote.js", '$__$.js'
				),
				"css" => array(
					"summernote.css"
				)
			),
			"jquery-ui" => array(
				"js" => array(
					"jquery-ui.js"
				),
				"css" => array(
					"jquery-ui.css",
					"jquery-ui.theme.css"
				)
			),
			"tinymce" => array(
				"js" => array(
					"tinymce.min.js"
				)
			),
			"flot" => array(
				"js" => array(
					"jquery.flot.min.js", 
			        "jquery.flot.resize.min.js", 
			        "jquery.flot.pie.min.js", 
			        "jquery.flot.stack.min.js", 
			        "jquery.flot.crosshair.min.js", 
			        "jquery.flot.categories.min.js", 
			        "jquery.flot.time.min.js",
			        '$__$.js' 
				)
			),
			"jquery.rateit" => array(
				"js" => array(
					"jquery.rateit.js",
					'$__$.js'
				),
				"css" => true
			),
			"colorpicker" => array(
				"js" => array(
					"colorpicker.js",
					'$__$.js'
				),
				"css" => array(
					"colorpicker.css"
				)
			),
			"jquery.jsonview" => array(
				"js" => array(
					"jquery.jsonview.js"
				),
				"css" => array(
					"jquery.jsonview.css"
				)
			),
			"a-list" => array(
				"js" => array(
					"list.js"
				),
				"css" => array(
					"list.css"
				),
				"dependency" => array(
					
				)
			),
			"admin-table" => array(
				"css" => array(
					"admin-table.css"
				)
			),
			"icheck" => array(
				"js" => array(
					"icheck.js", '$__$.js'
				),
				"css" => array(
					"square/blue.css"
				)
			)
		)
	),
	"__new_theme__" => array(
		"__autoloadBase" => true,
		"__autoloadExtensions" => array(),
		"__autoloadCustom" => array(
			"js" => array(),
			"css" => array()
		),
		"extensions" => array()
 	),
	"au" => array(
		"__autoloadBase" => true,
		"__autoloadExtensions" => array(
			
		),
		"extensions" => array(
			"au" => array(
				"js" => array(),
				"css" => array()
			)
		)
 	),
	"simple" => array(
		"__autoloadBase" => true,
		"__autoloadExtensions" => array(),
		"__autoloadCustom" => array(
			"js" => array(),
			"css" => array()
		),
		"extensions" => array()
 	),
	"metronic" => array(
		"__autoloadBase" => true,
		"__autoloadExtensions" => array(
			"metronic"
		),
		"__autoloadCustom" => array(
			"js" => array(),
			"css" => array()
		),
		"extensions" => array(
			"metronic" => array(
				"css" => array(
					"style-metronic.css", "style.css", "style-responsive.css", "themes/default.css", "custom.css"
				),
				"js" => array(
					"app.js"
				)
			),
			"bootstrap" => array(
				"css" => array(
					"bootstrap.min.css",
				),
				"js" => array(
					"bootstrap.js"
				)
			),
			"data-tables" => array(
				"css" => array(
					"DT_bootstrap.css"
				)
			)
		)
 	),
 	"api" => array(
 		"__autoloadBase" => true,
		"__autoloadExtensions" => array(
			"jquery.jsonview"
		),
		"__autoloadCustom" => array(
			"js" => array(),
			"css" => array()
		),
		"extensions" => array()
 	)
);