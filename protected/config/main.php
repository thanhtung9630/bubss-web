<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.

require_once(dirname(__FILE__) . '/../extensions/Son/helpers/global-functions/functions.php');

return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'AU',
	'defaultController' => 'member', 
	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.models.forms.*',
		'application.models.others.*',
		'application.components.*',
		'application.components.menu.*',
		'application.components.list.*',
		'application.components.form.*',
		'application.components.input.*',
		"ext.*",
		'ext.giix-components.*', // giix components
		'ext.YiiMailer.YiiMailer',
		"ext.Son.Son",
		"ext.Son.components.*",
		"ext.Son.helpers.*",
		"ext.Son.html.*",
		"ext.Son.models.*",
		"ext.Son.super-modules.list.*"
	),

	'modules'=>array(
		// uncomment the following to enable the Gii tool
		
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'tungdeptrai',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1'),
                        'generatorPaths' => array(
				'ext.giix-core', // giix generators
			),

		),
		
	),

	// application components
	'components'=>array(
		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
			'class'=>'WebUser',
			'autoUpdateFlash'=>false
		),

		"cache" => array(
			'class'=>'system.caching.CFileCache',
			"cachePath" => "_cache_data"
		),
		
		'urlManager'=>array(
			'urlFormat'=>'path',
			'showScriptName'=>false,
			'rules'=>array(
			  '<controller:\w+>/<id:\d+>'=>'<controller>/view',
			  '<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
			  '<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
		),
		
		'db'=>include(dirname(__FILE__).'/db.php'),
		
		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
				
				array(
					'class'=>'CWebLogRoute',
					"enabled" => isset($_GET["log"])
				),
				array(
	                'class'=>'CFileLogRoute',
	                'categories'=>'system.db.*',
	                'logFile'=>'sql.log',
	            ),
			),
		)
	),
	'params'=>include(dirname(__FILE__).'/params.php'),
	'theme'=>'simple',
	"language" => "en"
);