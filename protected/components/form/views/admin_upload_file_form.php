<div class="row">
	<div class="col-lg-12">
		<div class="text-center"><h3>Gửi file</h3></div>
		<?php $form->open(array(
			"id" => "form-upload-file"
		)); ?>
			<?php if($error = $form->getFirstError()): ?>
				<div class="alert alert-danger">
					<span>
						<?php echo $error; ?>
					</span>
				</div>
			<?php endif; ?>
			<div>
				<h4>Files</h4>
				<div class="row">
					<div class="col-lg-12">
						<?php $form->inputField("files",array(
							"class" => "form-control"
						)) ?>
					</div>
				</div>
			</div>
			<div style="margin-top: 50px;">
				<div class="row">
					<div class="col-lg-12">
						<?php $form->inputField("member_ids",array(
							"class" => "form-control"
						)) ?>
					</div>
				</div>
			</div>
			<div style="margin-top: 20px;">
				<h4>Description</h4>
				<div class="row">
					<div class="col-lg-12">
						<?php $form->inputField("description",array(
							"class" => "form-control",
							"rows" => 3
						)) ?>
					</div>
				</div>
			</div>
			<div class="text-right" style="margin-top: 10px">
				<?php $form->submitButton($form->viewParam("submitText",'Gửi file'),$form->viewParam("submitHtmlAttributes",array(
					"class" => "btn blue"
			))); ?>
			</div>
		<?php $form->close(); ?>
	</div>
</div>