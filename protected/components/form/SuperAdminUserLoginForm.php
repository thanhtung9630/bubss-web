<?php
class SuperAdminUserLoginForm extends SForm {
	var $config = array(
		"title" => "Đăng nhập",
		"inputs" => array(
			"__item" => array(
			),
			"login_name" => array(
				"type" => "text",
				"label" => "Tên đăng nhập",
				"rules" => array(
					array("required")
				)
			),
			"password" => array(
				"type" => "password",
				"label" => "Mật khẩu",
				"rules" => array(
					array("required")
				)
			)
		),
		"view" => "webroot.themes.metronic.views.components.form.form_login",
		"method" => "post"
	);

	protected function onHandleInput(){
		if(Input::isPost())
		{
			$valid = $this->readInput();
			$this->setError(!$valid);
			if($valid){
				$identity = new SuperAdminUserIdentity($this->login_name,$this->password);
				$result = $identity->authenticate();
				$this->setError(!$result);
				if($result){
					$duration= 3600 * 24 * 7; // 7 days
					Yii::app()->user->login($identity,$duration);
					Util::controller()->redirect("/superadmin");
				} else {
					$this->addError("global","Tên đăng nhập hoặc mật khẩu không đúng!");
				}
			}
			return true;
		}
		else 
		{
			return false;
		}
	}
}