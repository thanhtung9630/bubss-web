<?php
class AdminUploadFileForm extends SForm {
	protected function getConfig(){
		$config = array(
			"title" => "Gửi File đến thành viên",
			"inputs" => array(
				"__item" => array(
				),
				"files" => array(
					"label" => "Files",
					"type" => "file_picker",
					"config" => array(
						"multiple" => true
					)
				),
				"member_ids" => array(
					"label" => "Members",
					"type" => "members_picker",
					"config" => array(
						"admin_id" => Yii::app()->user->id,
						"multiple" => true
					)
				),
				"description" => array(
					"label" => "Description",
					"type" => "textarea"
				)
			),
			"view" => "application.components.form.views.admin_upload_file_form",
			"method" => "post",
			"uploadEnabled" => true,
		);
		return $config;
	}

	public function init(){
		$sPlugin = Son::load("SPlugin");
		$sPlugin->registerInputPlugin("members_picker","file","application.components.input.members_picker_input");
		parent::init();
	}

	protected function onHandleInput(){
		if(Input::isPost())
		{
			$valid = $this->readInput();
			$this->setError(!$valid);
			if($valid){
				if(!is_array($this->member_ids) || !count($this->member_ids)){
					$this->addError("global","You must select at least 1 member");
					$this->setError(true);
				} else {
					$inputFiles = ModelFile::getFileArrayFromInput("files","EncryptModelFile");
					//print_r($inputFiles); die();
					foreach($inputFiles as $file) {
						$file->key = "file";
					}
					$mainFiles = array();
					$mainFilesFiles = array();
					$members = array();
					foreach($this->member_ids as $member_id){
						$member = User::model()->findByPk($member_id,"active = 1 and admin_id = " . Yii::app()->user->id);
						if(!$member){
							$this->addError("global","Invalid members!");
							$this->setError(true);
							return true;
						}
						$members[$member_id] = $member;
					}
					foreach($inputFiles as $i => $inputFile){
						if($inputFile->error){
							$this->addError("global","File " . $inputFile->name . " is invalid. Please choose another file!");
							$this->setError(true);
							return true;
						}
						$mainFilesFiles[$i] = array();
						$mainFile = new MainFile();
						$mainFile->admin_id = Yii::app()->user->id;
						$mainFile->file_name = $inputFile->getLabelName();
						$mainFile->description = $this->description;
						$result = $mainFile->validate(array(
							"admin_id", "file_name", "description"
						));
						if(!$result){
							$this->addError("global",$mainFile->getFirstError());
							$this->setError(true);
							return true;
						}
						foreach($this->member_ids as $member_id){
							$file = new File();
							$file->member_id = $member_id;
							$file->fileList["file"] = $inputFile;
							$inputFile->model = $file;
							$result = $file->validate(array(
								"member_id"
							));
							if(!$result){
								$this->addError("global",$file->getFirstError());
								$this->setError(true);
								return true;
							}
							$mainFilesFiles[$i][] = $file;
						}
						$mainFiles[] = $mainFile;
					}
					foreach($mainFiles as $i => $mainFile){
						$mainFile->save(false);
						foreach($mainFilesFiles[$i] as $file){
							//$file->file = $inputFiles[$i];
							$inputFiles[$i]->model = $file;
							$file->main_file_id = $mainFile->id;
							$file->save(false);
						}
					}
				}
			}
			return true;
		}
		else 
		{
			return false;
		}
	}
}