<?php
class UserIdentity extends CUserIdentity {
	var $_id;

	public function authenticate(){
		$user = User::model()->findByAttributes(array(
			"email" => $this->username
		));
		if(!$user || !$user->active)
			return false;
		if(!$user->verifyPassword($this->password))
			return false;

		$this->_id = $user->id;

        $this->setState("type",$user->admin_id ? "member" : "admin");
		$this->setState('id', $user->id);
        $this->setState('name', $user->name);
		return true;
	}

	public function getId(){
		return $this->_id;
	}
}