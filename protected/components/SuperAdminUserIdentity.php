<?php
class SuperAdminUserIdentity extends CUserIdentity {
	var $_id;

	public function authenticate(){
		$admin = Util::param("super_admin");
		if($admin["email"]!=$this->username || $admin["password"]!=$this->password){
			return false;
		}

		$this->_id = $admin["email"];

        $this->setState("type","superadmin");
		$this->setState('id', $admin["email"]);
        $this->setState('name', $admin["email"]);
		return true;
	}

	public function getId(){
		return $this->_id;
	}
}