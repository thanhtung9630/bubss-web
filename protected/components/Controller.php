<?php
class Controller extends SController {
	var $permission = null;

	protected function beforeAction($action){
		if(!parent::beforeAction($action))
			return false;
		$this->permission = new Permission();
		include_once(dirname(__FILE__) . "/code/" . "before_action.php");
		return true;
	}
}