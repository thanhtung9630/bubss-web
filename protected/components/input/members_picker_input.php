<?php // @var $name, $value, $config, $htmlAttributes ?>
<?php
	$memberList = new MemberList();
	if(($adminId = ArrayHelper::get($config,"admin_id",null))!==null){
		$memberList->addCondition(array(
			"admin_id" => $adminId
		));
	}
	$uniqueIndex = Util::generateUniqueStringByRequest();
	$divId = "members-picker-$uniqueIndex";
?>
<style>
	#<?php echo $divId ?> [list-item]{
		cursor: pointer;
	}

	#<?php echo $divId ?> [list-item]:hover {
		color: rgb(40,151,199);
	}

	#<?php echo $divId ?> [list-item].active > * {
		border-color: rgb(40,104,199) !important;
	}
	<?php echo $divId ?> .customer-name {
		
	}
</style>
<div id="<?php echo $divId ?>">
	<?php $htmlAttributes["class"] = "members-input"; ?>
	<?php //echo CHtml::hiddenField($name,$value,$htmlAttributes); ?>
	<div class="portlet box purple">
		<div class="portlet-title">
			<div class="caption">Chọn thành viên<span class="customer-name bold italic underline"></span></div>
		</div>
		<div class="portlet-body">
			<?php $memberList->getHtml()->render(); ?>
		</div>
	</div>
</div>
<script>
	var listManager = SList.ListManager.getInstance();
	var listAlias = "<?php echo $memberList->alias ?>";
	listManager.listen(listAlias,"list-selected-items-changed",function(obj){
		var $div = $("#<?php echo $divId ?>");
		var selectedMembers = obj.list.getSelectedObjects();
		$div.find(".members-input").remove();
		$.each(selectedMembers,function(k,member){
			$div.append('<input type="hidden" name="<?php echo $name ?>" value="'+member.id+'" class="members-input">');
		});
	});
</script>