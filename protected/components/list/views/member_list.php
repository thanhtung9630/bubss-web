<style>
	[list-id="<?php echo $list->alias ?>"] [list-items] > [list-item]:nth-child(n+4) {
		margin-top: 10px;
	}
</style>
<?php $html->begin(); ?>
	<div class="row">				
		<div class="col-lg-3 text-left">
			<?php $html->renderSearchInput(array(
				"class" => "form-control"
			)); ?>
		</div>
		<div class="col-lg-3 col-lg-offset-6 text-right">
			<button  type="button" list-do-search class="btn btn-primary">Search</button>
		</div>
	</div>
	<hr/>
	<div class="row">
		<div class="col-lg-12">
			<div class="row" list-items>
				<?php $html->resetLoop(); while($html->loop()): ?>
					<?php $html->renderCurrentItem() ?>
				<?php endwhile; ?>
			</div>
		</div>
	</div>
<?php $html->end(); ?>