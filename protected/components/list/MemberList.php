<?php
class MemberList extends SList {
	protected function getConfig(){
		$arr = array(
			"fields" => array(
				"__item" => array(
					"order" => true
				),
				"id" => array(
					"label" => "ID"
				),
				"created_time" => array(
					"label" => "Created Time",
					"displayType" => "timestamp",
				),
				"updated_time" => array(
					"label" => "Updated Time",
					"displayType" => "timestamp",
				),
				"active" => array(
					"label" => "Active",
				),
				"name" => array(
					"label" => "Name",
					"advancedSearchInputType" => "text_match_partial",
					"advancedSearchHtmlAttributes" => array(
						"placeholder" => "Name"
					)
				),
				"email" => array(
					"label" => "Email",
					"advancedSearchInputType" => "text_match_partial",
					"advancedSearchHtmlAttributes" => array(
						"placeholder" => "Email"
					)
				),
			),
			"actions" => array(
				"action" => array(
					"data" => array(
						"search" => array(
							"id", "name", "email",
						),
						"advancedSearch" => true,
						"limit" => 20,
						"page" => true,
					),
				),
			),
			"model" => array(
				"class" => "User",
				"primaryField" => "id",
				"conditions" => array(
					"active" => 1
				),
				"with" => array(),
				"addedCondition" => array(),
				"defaultQuery" => array(
					"orderBy" => "id",
					"orderType" => "desc",
					"limit" => 20,
					"offset" => 0,
					"search" => "",
					"advancedSearch" => array(
						"active" => 1
					),
					"page" => 1
				),
				"dynamicInputs" => array(
				),
				"preloadData" => false
			),
			"view" => array(
				"viewPath" => array(
					"list" => "application.components.list.views.member_list",
					"item" => "application.components.list.views.member_item",
				),
				"itemSelectable" => array(
					"type" => "click",
					"multiple" => true,
					"selectedClass" => "active",
				),
				"trackUrl" => false
			),
			"mode" => "jquery",
			"baseUrl" => Util::controller()->createUrl("/admin/member_list?")
		);

		return $arr;
	}
}