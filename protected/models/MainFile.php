<?php

Yii::import('application.models._base.BaseMainFile');

class MainFile extends BaseMainFile
{
	public $num_file;

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function relations(){
		return array(
			"admin" => array(
				self::BELONGS_TO, "User", "admin_id"
			),
			"files" => array(
				self::HAS_MANY, "File", "main_file_id"
			),
			"file_count" => array(
				self::STAT, "File", "main_file_id"
			)
		);
	}

	protected function afterDelete(){
		parent::afterDelete();
		foreach($this->files as $file){
			$file->delete();
		}
	}
}