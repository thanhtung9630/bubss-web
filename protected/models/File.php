<?php

Yii::import('application.models._base.BaseFile');

class File extends BaseFile
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function relations(){
		return array(
			"member" => array(
				self::BELONGS_TO, "User", "member_id"
			),
			"main_file" => array(
				self::BELONGS_TO, "MainFile", "main_file_id"
			)
		);
	}

	protected $fileUploadEnabled = true;
	protected function getFileConfig(){
		return array(
			"file" => array(
				"folder" => function($model){
					return "upload/user/" . $model->member_id;
				},
				"fileName" => function($model){
					return $model->main_file->file_name . "_" . time();
				},
				"updateFileNameAfterSave" => false
			),
		);
	}

	
}