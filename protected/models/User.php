<?php

Yii::import('application.models._base.BaseUser');

class User extends BaseUser
{
	const SCENARIO_CREATE_RANDOM_CODE = "SCENARIO_CREATE_RANDOM_CODE";

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	protected $passwordField = "password";

	protected function securityToBeXssCleanProperties(){
		return array("name", "email");
	}

	protected $fileUploadEnabled = true;
	protected function fileGetFolderToBeDeleted(){
		return "upload/user/" . $this->id;
	}

	public function relations(){
		return array(
			"admin" => array(
				self::BELONGS_TO, "User", "admin_id"
			),
			"files" => array(
				self::HAS_MANY, "File", "member_id"
			),
			"main_files" => array(
				self::HAS_MANY, "MainFile", "admin_id"
			),
			"members" => array(
				self::HAS_MANY, "User", "admin_id"
			)
		);
	}

	protected function afterDelete(){
		parent::afterDelete();
		if($this->admin_id){
			foreach($this->files as $file){
				$file->delete();
			}
		} else {
			foreach($this->members as $member){
				$member->delete();
			}
			foreach($this->main_files as $mainFile){
				$mainFile->delete();
			}
		}
	}

	protected function afterSave(){
		parent::afterSave();
		if($this->getIsNewRecord()){
			$this->generateRandomCode();
		}
	}

	public function generateRandomCode($saveToDb=true){
		if($saveToDb && $this->scenario==self::SCENARIO_CREATE_RANDOM_CODE)
			return;
		$this->random_code = Util::generateRandomString(6,substr(md5($this->id . time()),0,10));
		if(!$saveToDb)
			return;
		$this->setIsNewRecord(false);
		$this->scenario = self::SCENARIO_CREATE_RANDOM_CODE;
		$result = $this->save(true,array(
			"random_code"
		));
	}

	public function generateAccessToken(){
		$this->access_token = Util::generateRandomString(30,md5($this->id . time()));
		$this->save(true,array(
			"access_token"
		));
	}
}