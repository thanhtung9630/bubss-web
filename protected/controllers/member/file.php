<?php
$arr = array(
	"fields" => array(
		"__item" => array(
			"order" => true
		),
		"id" => array(
			"label" => "ID"
		),
		"created_time" => array(
			"label" => "Created Time",
			"displayType" => "timestamp",
			"inputType" => "timestamp_datetimepicker",
			"advancedSearchInputType" => "timestamp_range_datetimepicker",
			"exportType" => "timestamp"
		),
		"updated_time" => array(
			"label" => "Updated Time",
			"displayType" => "timestamp",
			"inputType" => "timestamp_datetimepicker",
			"advancedSearchInputType" => "timestamp_range_datetimepicker",
			"exportType" => "timestamp"
		),
		"active" => array(
			"label" => "Active",
			"inputType" => "checkbox_button",
			"displayType" => "checkbox_label",
			"advancedSearchInputType" => true,
			"advancedSearchConfig" => array(
				"triggerType" => "changed",
			),
			"exportType" => "boolean"
		),
		"file" => array(
			"label" => "File"
		),
		"file_name" => array(
			"foreignConfig" => array("main_file","file_name"),
			"label" => "File name",
			"advancedSearchInputType" => "text_match_partial",
			"advancedSearchHtmlAttributes" => array(
				"placeholder" => "File name"
			)
		),
		"description" => array(
			"foreignConfig" => array("main_file","description"),
			"label" => "Description",
		)
	),
	"actions" => array(
		"action" => array(
			"update" => false,
			"delete" => true,
			"insert" => false,
			"data" => array(
				"search" => array(
					"id", "file_name", "description"
				),
				"advancedSearch" => true,
				"order" => true,
				"limit" => true,
				"offset" => true,
				"page" => true,
				"export" => false,
			),
		),
	),
	"model" => array(
		"class" => "File",
		"primaryField" => "id",
		"conditions" => array(
			"member_id" => Yii::app()->user->id,
			"t.active" => 1
		),
		"with" => array(
			"main_file"
		),
		"addedCondition" => array(),
		"defaultQuery" => array(
			"orderBy" => "id",
			"orderType" => "desc",
			"limit" => 20,
			"offset" => 0,
			"search" => "",
			"advancedSearch" => array(
				"active" => 1
			),
			"page" => 1
		),
		"dynamicInputs" => array(
		),
		"preloadData" => false,
	),
	"view" => array(
		"itemSelectable" => false,
		"onRender" => function($list){
			$sAsset = Son::load("SAsset");
			$sAsset->startCssCode();
			?>
			<style>
				.list-th-actions {
					width:200px;
				}

				.list-th-attr-id {
					width:60px;
				}
				.list-th-attr-active {
					width:80px;
				}
			</style>
			<?php
			$sAsset->endCssCode();
		},
		"limitSelectList" => array(10,20,30,40),
		"trackUrl" => true
	),
	"pagination" => array(
		"first" => true,
		"back" => true,
		"next" => true,
		"last" => true,
		"count" => 5
	),
	"admin" => array(
		"title" => "File Management",
		"columns" => array(
			"id", "file_name", "created_time", "active"
		),
		"detail" => array(
			"id", "created_time", "updated_time", "active", "file_name", "description"
		),
		"action" => true,
		"actionButtons" => array(
			array("link",function($item){
				return array(
					"href" => $item->model->url("file"),
					"content" => 'Download <i class="fa fa-cloud-download"></i>',
					"newTab" => false
				);
			})
		)
	),
	"mode" => "jquery",
);

return $arr;