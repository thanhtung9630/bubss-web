<?php
Yii::import("ext.Son.super-modules.admin-table.*");
class MemberController extends AdminTableBaseController {
	protected function getConfig(){ 
		$arr = array(
			"pageList" => array(
				"file" => "File Manager"
			),
			"menu" => array(
				"file" => array(
					"content" => "File Manager",
					"icon" => "fa fa-file-o",
				),
			),
			"folderName" => "member",
			"defaultPage" => "file",
			"authenticate" => function($controller){
				if(in_array($controller->action->id,array(
					"login", "logout"
				))){
					return true;
				}
				$loggedIn = !Yii::app()->user->isGuest && Yii::app()->user->getState("type")=="member";
				if(!$loggedIn){
					$controller->redirect("/member/login");
				}
				return $loggedIn;
			},
			"view" => array(
				"layout" => "webroot.themes.metronic.views.layouts.layout_sidebar"
			),
			"listDefaultConfig" => array(
				"view" => array(
					"viewPath" => array(
						"list" => "webroot.themes.metronic.views.admin.list",
						"item" => "webroot.themes.metronic.views.admin.item",
					)
				)
			)
		);
		return $arr;
	}

	protected function getCurrentFile(){
		return __FILE__;
	}

	public function getBaseUrl($path=""){
		return $this->createAbsoluteUrl("/member/") . "/" . $path . "/";
	}

	public function init(){
		Yii::app()->theme = "metronic";
		$this->data["brandName"] = 'Member';
		parent::init();
	}
	// actions

	public function actionLogin(){
		$this->layout = "webroot.themes.metronic.views.layouts.layout_login";
		$form = Son::load("UserLoginForm");
		if($form->handle() && !$form->isError())
		{
			return;
		}
		$form->renderPage();
	}

	public function actionLogout(){
		Yii::app()->user->logout();
		$this->redirect("/member/login");
	}
}