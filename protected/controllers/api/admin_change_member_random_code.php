<?php
return array(
	array(
		"inputs" => array(
			"access_token" => array(
				"willRead" => false
			),
			"member_id" => array(
				"rules" => array(
					array("required")
				)
			)
		),
		"authenticate" => "access_token",
		"outputExample" => array(
			"error" => 0,
			"message" => "Ok",
			"data" => array(
				"random_code" => "78910JQKA"
			)
		)
	),
	function($controller){
		list($member_id) = $controller->readInput();
		// Do something
		$member = User::model()->findByAttributes(array(
			"id" => $member_id,
			"active" => 1,
			"admin_id" => $controller->user->id
		));
		if(!$member){
			Output::returnJsonError("Member doesnot exist");
			return;
		}
		$member->generateRandomCode();
		// Output
		Output::returnJsonSuccess(array(
			"random_code" => $member->random_code
		));
	}
);