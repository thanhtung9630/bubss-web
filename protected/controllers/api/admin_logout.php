<?php
return array(
	array(
		"inputs" => array(
			"access_token" => array(
				"willRead" => false
			),
		),
		"authenticate" => "access_token",
		"outputExample" => array(
			"error" => 0,
			"message" => "Ok"
		)
	),
	function($controller){
		$controller->readInput();
		// Do something
		$controller->user->access_token = "";
		$controller->user->save(true,array(
			"access_token"
		));
		// Output
		Output::returnJsonSuccess();
	}
);