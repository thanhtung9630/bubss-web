<?php
return array(
	array(
		"inputs" => array(
			"email" => array(
				"rules" => array(
					array("required"),
					array("email", "allowEmpty" => false),
					array("length", "min" => 3)
				)
			),
			"password" => array(
				"rules" => array(
					array("required"),
					array("length", "min" => 3)
				)
			)
		),
		"outputExample" => array(
			"error" => 0,
			"message" => "Ok",
			"data" => array(
				"user_id" => 1,
				"access_token" => "78910JQKA"
			)
		)
	),
	function($controller){
		list($email,$password) = $controller->readInput();
		// Do something
		$admin = User::model()->findByAttributes(array(
			"email" => $email,
			"active" => 1,
			"admin_id" => 0
		));
		if(!$admin || !$admin->verifyPassword($password)){
			Output::returnJsonError("Invalid login information");
			return;
		}
		$admin->generateAccessToken();
		// Output
		Output::returnJsonSuccess(array(
			"user_id" => $admin->id,
			"access_token" => $admin->access_token
		));
	}
);