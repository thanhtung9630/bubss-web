<?php
return array(
	array(
		"inputs" => array(
			"access_token" => array(
				"willRead" => false
			)
		),
		"authenticate" => "access_token",
		"outputExample" => array(
			"error" => 0,
			"message" => "Ok",
			"data" => array(
				array(
					"id" => 1,
					"name" => "Nguyen Thanh Tung",
					"email" => "thanhtung9630@gmail.com"
				)
			)
		)
	),
	function($controller){
		$controller->readInput();
		// Do something
		$members = User::model()->findAllByAttributes(array(
			"admin_id" => $controller->user->id,
			"active" => 1
		));
		$returnArr = array();
		foreach ($members as $member) {
			$returnArr[] = array(
				"id" => $member->id,
				"name" => $member->name,
				"email" => $member->email
			);
		}
		// Output
		Output::returnJsonSuccess($returnArr);
	}
);