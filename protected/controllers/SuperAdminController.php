<?php
Yii::import("ext.Son.super-modules.admin-table.*");
class SuperAdminController extends AdminTableBaseController {
	protected function getConfig(){ 
		$arr = array(
			"pageList" => array(
				"admin" => "Admin Manager",
			),
			"menu" => array(
				"admin" => array(
					"content" => "Admin Manager",
					"icon" => "fa fa-key",
				),
			),
			"folderName" => "super_admin",
			"defaultPage" => "admin",
			"authenticate" => function($controller){
				if(in_array($controller->action->id,array(
					"login", "logout"
				))){
					return true;
				}
				$loggedIn = !Yii::app()->user->isGuest && Yii::app()->user->getState("type")=="superadmin";
				if(!$loggedIn){
					$controller->redirect("/superadmin/login");
				}
				return $loggedIn;
			},
			"view" => array(
				"layout" => "webroot.themes.metronic.views.layouts.layout_sidebar"
			),
			"listDefaultConfig" => array(
				"view" => array(
					"viewPath" => array(
						"list" => "webroot.themes.metronic.views.admin.list",
						"item" => "webroot.themes.metronic.views.admin.item",
					)
				)
			)
		);
		return $arr;
	}

	protected function getCurrentFile(){
		return __FILE__;
	}

	public function getBaseUrl($path=""){
		return $this->createAbsoluteUrl("/superadmin/") . "/" . $path . "/";
	}

	public function init(){
		Yii::app()->theme = "metronic";
		$this->data["brandName"] = 'Super Admin';
		parent::init();
	}
	// actions

	public function actionLogin(){
		$this->layout = "webroot.themes.metronic.views.layouts.layout_login";
		$form = Son::load("SuperAdminUserLoginForm");
		if($form->handle() && !$form->isError())
		{
			return;
		}
		$form->renderPage();
	}

	public function actionLogout(){
		Yii::app()->user->logout();
		$this->redirect("/superadmin/login");
	}
}