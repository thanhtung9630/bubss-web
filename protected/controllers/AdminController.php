<?php
Yii::import("ext.Son.super-modules.admin-table.*");
class AdminController extends AdminTableBaseController {
	protected function getConfig(){ 
		$arr = array(
			"pageList" => array(
				"member" => "Member Manager",
				"main_file" => "File Manager"
			),
			"menu" => array(
				"member" => array(
					"content" => "Member Manager",
					"icon" => "fa fa-users",
				),
				"main_file" => array(
					"content" => "File Manager",
					"icon" => "fa fa-file-o",
				),
			),
			"folderName" => "admin",
			"defaultPage" => "member",
			"authenticate" => function($controller){
				if(in_array($controller->action->id,array(
					"login", "logout"
				))){
					return true;
				}
				$loggedIn = !Yii::app()->user->isGuest && Yii::app()->user->getState("type")=="admin";
				if(!$loggedIn){
					$controller->redirect("/admin/login");
				}
				return $loggedIn;
			},
			"view" => array(
				"layout" => "webroot.themes.metronic.views.layouts.layout_sidebar"
			),
			"listDefaultConfig" => array(
				"view" => array(
					"viewPath" => array(
						"list" => "webroot.themes.metronic.views.admin.list",
						"item" => "webroot.themes.metronic.views.admin.item",
					)
				)
			)
		);
		return $arr;
	}

	protected function getCurrentFile(){
		return __FILE__;
	}

	public function getBaseUrl($path=""){
		return $this->createAbsoluteUrl("/admin/") . "/" . $path . "/";
	}

	public function init(){
		Yii::app()->theme = "metronic";
		$this->data["brandName"] = 'Admin';
		$this->layout = "webroot.themes.metronic.views.layouts.layout_sidebar";
		parent::init();
	}
	// actions

	public function actionAdd_file(){
		$form = Son::load("AdminUploadFileForm");
		if($form->handle() && !$form->isError())
		{
			Util::controller()->redirect("/admin/main_file");
			return;
		}
		$form->renderPage();
	}

	public function actionMember_list(){
		$memberList = Son::load("MemberList");
		$memberList->addCondition(array(
			"admin_id" => Yii::app()->user->id
		));
		$memberList->run();
	}

	public function actionLogin(){
		$this->layout = "webroot.themes.metronic.views.layouts.layout_login";
		$form = Son::load("UserLoginForm");
		if($form->handle() && !$form->isError())
		{
			return;
		}
		$form->renderPage();
	}

	public function actionLogout(){
		Yii::app()->user->logout();
		$this->redirect("/admin/login");
	}
}