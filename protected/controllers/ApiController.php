<?php

Yii::import("ext.Son.super-modules.api.*");

class ApiController extends ApiBaseController {

    const INVALID_ACCESS_TOKEN_ERROR_CODE = 2;

    public $user = null;
    protected $config = array(
        "apiList" => array(
            "admin_login", "admin_logout", "admin_get_list_member", "admin_get_member_random_code", "admin_change_member_random_code",
        )
    );

    protected function getCurrentFile() {
        return __FILE__;
    }

    public function getBaseUrl($path = "") {
        return $this->createAbsoluteUrl("/api") . "/" . $path . "/";
    }

    protected function getAuthenticateMethods() {
        $self = $this;
        return array(
            "access_token" => function() use($self) {
                Output::setJsonErrorCode(2);
                $access_token = Input::post("access_token", array(
                            "rules" => array(
                                array("required"),
                                array("length", "min" => 1, "allowEmpty" => false)
                            )
                                ), "json");
                Output::setJsonErrorCode(1);

                $user = User::model()->findByAttributes(array('access_token' => $access_token),"access_token is not null");

                if ($user) {
                    $self->user = $user;
                    return true;
                } else {
                    Output::returnJsonError("Invalid access token", 2);
                    return false;
                }
            }
        );
    }

}
        