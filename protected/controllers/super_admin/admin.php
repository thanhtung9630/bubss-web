<?php
$arr = array(
	"fields" => array(
		"__item" => array(
			"order" => true
		),
		"id" => array(
			"label" => "ID"
		),
		"created_time" => array(
			"label" => "Created Time",
			"displayType" => "timestamp",
			"inputType" => "timestamp_datetimepicker",
			"advancedSearchInputType" => "timestamp_range_datetimepicker",
			"exportType" => "timestamp"
		),
		"updated_time" => array(
			"label" => "Updated Time",
			"displayType" => "timestamp",
			"inputType" => "timestamp_datetimepicker",
			"advancedSearchInputType" => "timestamp_range_datetimepicker",
			"exportType" => "timestamp"
		),
		"active" => array(
			"label" => "Active",
			"inputType" => "checkbox_button",
			"displayType" => "checkbox_label",
			"advancedSearchInputType" => true,
			"advancedSearchConfig" => array(
				"triggerType" => "changed",
			),
			"exportType" => "boolean"
		),
		"name" => array(
			"label" => "Name",
			"advancedSearchInputType" => "text_match_partial",
			"advancedSearchHtmlAttributes" => array(
				"placeholder" => "Name"
			)
		),
		"email" => array(
			"label" => "Email",
			"advancedSearchInputType" => "text_match_partial",
			"advancedSearchHtmlAttributes" => array(
				"placeholder" => "Login name"
			)
		),
		"password" => array(
			"label" => "Password",
			"inputType" => "password"
		),
		"admin_id" => array(
			"default" => 0,
			"displayInput" => false
		)
	),
	"actions" => array(
		"action" => array(
			"update" => array(
				"active", "name", "email", "password",
			),
			"delete" => true,
			"insert" => array(
				"name", "email", "password", "admin_id"
			),
			"data" => array(
				"search" => array(
					"id", "name", "email",
				),
				"advancedSearch" => true,
				"order" => true,
				"limit" => true,
				"offset" => true,
				"page" => true,
				"export" => false,
			),
		),
		"actionTogether" => array(
			"deleteTogether" => false	
		),
		"extendedAction" => array(
		),
		"extendedActionTogether" => array(
		),
	),
	"model" => array(
		"class" => "User",
		"primaryField" => "id",
		"conditions" => array(
			"admin_id" => 0
		),
		"with" => array(),
		"addedCondition" => array(),
		"defaultQuery" => array(
			"orderBy" => "id",
			"orderType" => "desc",
			"limit" => 20,
			"offset" => 0,
			"search" => "",
			"advancedSearch" => array(
				"active" => 1
			),
			"page" => 1
		),
		"dynamicInputs" => array(
		),
		"preloadData" => false
	),
	"view" => array(
		"itemSelectable" => false,
		"onRender" => function($list){
			$sAsset = Son::load("SAsset");
			$sAsset->startCssCode();
			?>
			<style>
				.list-th-actions {
					width:200px;
				}

				.list-th-attr-id {
					width:60px;
				}
				.list-th-attr-active {
					width:80px;
				}
			</style>
			<?php
			$sAsset->endCssCode();
		},
		"limitSelectList" => array(10,20,30,40),
		"trackUrl" => true
	),
	"pagination" => array(
		"first" => true,
		"back" => true,
		"next" => true,
		"last" => true,
		"count" => 5
	),
	"admin" => array(
		"title" => "Admin Management",
		"columns" => array(
			"id", "name", "email", "active"
		),
		"detail" => array(
			"id", "created_time", "updated_time", "active", "name", "email", 
		),
		"action" => true,
		"actionButtons" => array(
		)
	),
	"mode" => "jquery",
);

return $arr;