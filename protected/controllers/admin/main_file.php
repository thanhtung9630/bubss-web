<?php
$arr = array(
	"fields" => array(
		"__item" => array(
			"order" => true
		),
		"id" => array(
			"label" => "ID"
		),
		"created_time" => array(
			"label" => "Created Time",
			"displayType" => "timestamp",
			"inputType" => "timestamp_datetimepicker",
			"advancedSearchInputType" => "timestamp_range_datetimepicker",
			"exportType" => "timestamp"
		),
		"updated_time" => array(
			"label" => "Updated Time",
			"displayType" => "timestamp",
			"inputType" => "timestamp_datetimepicker",
			"advancedSearchInputType" => "timestamp_range_datetimepicker",
			"exportType" => "timestamp"
		),
		"active" => array(
			"label" => "Active",
			"inputType" => "checkbox_button",
			"displayType" => "checkbox_label",
			"advancedSearchInputType" => true,
			"advancedSearchConfig" => array(
				"triggerType" => "changed",
			),
			"exportType" => "boolean"
		),
		"file_name" => array(
			"label" => "File name",
			"advancedSearchInputType" => "text_match_partial",
			"advancedSearchHtmlAttributes" => array(
				"placeholder" => "File name"
			)
		),
		"description" => array(
			"label" => "Description"
		),
		"num_file" => array(
			"label" => "File count",
			"manualJoinForeignConfig" => array("t_num_file","num")
		)
	),
	"actions" => array(
		"action" => array(
			"update" => false,
			"delete" => true,
			"insert" => false,
			"data" => array(
				"search" => array(
					"id", "file_name"
				),
				"advancedSearch" => true,
				"order" => true,
				"limit" => true,
				"offset" => true,
				"page" => true,
				"export" => false,
			),
		),
	),
	"model" => array(
		"class" => "MainFile",
		"primaryField" => "id",
		"conditions" => array(
			"admin_id" => Yii::app()->user->id
		),
		"with" => array(),
		"addedCondition" => array(),
		"defaultQuery" => array(
			"orderBy" => "id",
			"orderType" => "desc",
			"limit" => 20,
			"offset" => 0,
			"search" => "",
			"advancedSearch" => array(
				"active" => 1
			),
			"page" => 1
		),
		"dynamicInputs" => array(
		),
		"preloadData" => false,
		"onCriteria" => function($criteria){
			$criteria->join .= " left join (select count(*) as num, main_file_id from {{file}} group by main_file_id) t_num_file on (t_num_file.main_file_id = t.id)";
		}
	),
	"view" => array(
		"itemSelectable" => false,
		"onRender" => function($list){
			$sAsset = Son::load("SAsset");
			$sAsset->startCssCode();
			?>
			<style>
				.list-th-actions {
					width:200px;
				}

				.list-th-attr-id {
					width:60px;
				}
				.list-th-attr-active {
					width:80px;
				}
			</style>
			<?php
			$sAsset->endCssCode();

			$list->getHtml()->htmlAt("table_actions",function() use($list){
				?>
					<a href="<?php echo Util::controller()->createUrl("/admin/add_file") ?>" class="btn red"><i class="fa fa-plus"></i> Add Files</a>
				<?php
			});
		},
		"limitSelectList" => array(10,20,30,40),
		"trackUrl" => true,
	),
	"pagination" => array(
		"first" => true,
		"back" => true,
		"next" => true,
		"last" => true,
		"count" => 5
	),
	"admin" => array(
		"title" => "File Management",
		"columns" => array(
			"id", "file_name", "num_file", "active"
		),
		"detail" => array(
			"id", "created_time", "updated_time", "active", "file_name", "description", "num_file",
		),
		"action" => true,
		"actionButtons" => array(
		)
	),
	"mode" => "jquery",
);

return $arr;