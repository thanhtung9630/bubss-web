<?php
class EncryptModelFile extends ModelFile {
	 public function save($filePath){
        $result = FileEncryptHelper::encryptFile($this->tmp_name, $filePath,$this->model->member->random_code);
        return $result;
    }

    public function generateFilePath(){
        $folder = $this->config["folder"];
        if(is_callable($folder)){
            $folder = $folder($this->model);
        }
        if (!file_exists($folder)) {
            mkdir($folder, 0777, true);
        }
        $fileName = $this->config["fileName"]($this->model);
        $filePath = $folder . "/" . $fileName . "." . $this->targetExtension . ".bubss";
        $this->savedFilePath = $filePath;
    }
}