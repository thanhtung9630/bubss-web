<?php
class FileEncryptHelper {
	const IV_ORIGIN = "laodonghangsay";

	public static function encryptFile($srcFilePath,$destFilePath,$password){
		$data = file_get_contents($srcFilePath);
		$passphrase = self::getPassphrase($password);
		$iv = self::getIv(self::IV_ORIGIN);
		$encryptedData = self::fnEncrypt($data,$passphrase,$iv);
		file_put_contents($destFilePath, $encryptedData);
		return true;
	}

	public static function encryptString($data,$password){
		$passphrase = self::getPassphrase($password);
		$iv = self::getIv(self::IV_ORIGIN);
		$encryptedData = self::fnEncrypt($data,$passphrase,$iv);
		return $encryptedData;
	}

	protected static function getPassphrase($passphraseOrigin){
		//return substr(hash("SHA256", $passphraseOrigin),0,16);
		return $passphraseOrigin;
	}

	protected static function getIv($ivOrigin){
		return substr(hash("SHA256",$ivOrigin),0,16);
	}

	protected static function fnEncrypt($sValue, $sSecretKey,$iv) {

    	//echo "$sValue<br/>$sSecretKey<br/>$iv<br/>";

		$block = mcrypt_get_block_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
    	$padding = $block - (strlen($sValue) % $block);
    	$sValue .= str_repeat("\0", $padding);

    	//$padding = 16 - (strlen($sValue) % 16);
  		//$sValue .= str_repeat(chr($padding), $padding);

    	$rawData = mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $sSecretKey, $sValue, MCRYPT_MODE_CBC, $iv);
		$data = base64_encode($rawData);
	    $data = rtrim($data, "\0\3");

		//echo "$sValue<br/>$rawData<br/>$data";

	    return $data;
	}

	protected static function fnDecrypt($sValue, $sSecretKey,$iv) {
		$data = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $sSecretKey, base64_decode($sValue), MCRYPT_MODE_CBC, $iv);
	    //$data = rtrim($data, "\0\3");
	    return $data;
	}
}