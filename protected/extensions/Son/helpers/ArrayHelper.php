<?php
class ArrayHelper {
	public static function get(&$arr,$key,$default=null,$setIfNotExist=false){
		if($arr==null || !is_array($arr) || !isset($arr[$key])){
			if(is_callable($default)){
				$default = $default();
			}
			if($setIfNotExist){
				if($arr==null){
					$arr = array();
				}
				$arr[$key] = $default;
			}
			return $default;
		}
		return $arr[$key];
	}

	public static function has($arr,$key){
		return $arr && isset($arr[$key]);
	}

	public static function set(&$arr,$key,$value="__unset__"){
		if($arr==null){
			$arr = array();
		}
		if($value=="__unset__"){
			$arr = array_merge_recursive($arr,$key);
			return;
		}
		$arr[$key] = $value;
	}

	public static function processItemDefault(&$arr,$default=null){
		if(!($defaultItem = ArrayHelper::get($arr,"__item",$default)))
			return;
		if(isset($arr["__item"]))
			unset($arr["__item"]);
		foreach($arr as $key => $item){
			$arr[$key] = array_replace_recursive($defaultItem,$item);
		}
	}

	public static function processItemDefaultAssoc(&$arr,$default=null){
		if(!($defaultItem = ArrayHelper::get($arr,"__item",$default)))
			return;
		if(isset($arr["__item"]))
			unset($arr["__item"]);
		foreach($arr as $key => $item){
			if(is_numeric($key)){
				$arr[$item] = $defaultItem;
				unset($arr[$key]);
			} else {
				$arr[$key] = array_replace_recursive($defaultItem,$item);
			}
		}
	}

	public static function asArray($item){
		if(is_array($item)){
			return $item;
		}
		return array($item);
	}
}