<?php
class Util {
    static $uniqueIndex;
    
    public static function urlAppendParams($url,$paramString){
        if(is_array($paramString)){
            $paramString = http_build_query($paramString);
        }
		$paramStringFirstCharIsAnd;
        if(!$paramString){
            $paramString = "";
			$paramStringFirstCharIsAnd = false;
        } else {
			$paramStringFirstCharIsAnd = $paramString[0]=="&";
		}
		
        $urlAlreadyHasParams = parse_url($url, PHP_URL_QUERY);
		$urlLastChar = $url ? $url[strlen($url)-1] : "";
		$urlLastCharIsQuestionMark = $urlLastChar=="?";
		$urlLastCharIsAnd = $urlLastChar=="&";
		
		if($urlAlreadyHasParams){
			if($urlLastCharIsAnd || $paramStringFirstCharIsAnd)
				$url = $url . $paramString;
			else
				$url = "$url&$paramString";
		} else {
			if($urlLastCharIsQuestionMark || $urlLastCharIsAnd)
				$url = $url . $paramString;
			else
				$url = "$url?$paramString";
		}
		
        return $url;
    }

    public static function controller(){
        return Yii::app()->controller;
    }

    public static function l($url,$echo=false){
        $url = Yii::app()->controller->createUrl($url);
        if($echo){
            echo $url;
        }
        return $url;
    }

    public function md($url,$echo=false){
        return self::l("/" . $this->module->getName() . "/" . $url,$echo);
    }

    public static function deleteFile($path)
    {
        if (is_dir($path) === true)
        {
            $files = array_diff(scandir($path), array('.', '..'));

            foreach ($files as $file)
            {
                self::deleteFile(realpath($path) . '/' . $file);
            }

            return rmdir($path);
        }
        else if (is_file($path) === true)
        {
            return unlink($path);
        }

        return false;
    }

	public static function generateRandomString($length = 10,$customAppend="") {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $randomString . $customAppend;
    }

    public static function generateUniqueStringByRequest(){
        return Util::$uniqueIndex++;
    }

    public static function sendMail($to,$subject,$view,$params)
    {
        if($view!="empty"){
            $template = Template::model()->findByName($view);
            if($template!=null){
                self::sendMailWithContent($to,$subject,$template->applyParams($params));
                return;
            }
        }

        $mail = new YiiMailer($view, $params);
                
        //set properties
        $setting = Yii::app()->params["accounts"]["mail"];
        $mail->setFrom($setting["adminEmail"], $setting["admin"]);
        $mail->setSubject($subject);
        $mail->setTo($to);

        $mail->IsSMTP();
        $mail->Host       = $setting["host"];
        $mail->SMTPDebug  = 0;                     
        $mail->SMTPAuth   = true;
        $mail->SMTPSecure = $setting["smtp"]["secure"];
        $mail->Host       = $setting["smtp"]["host"];
        $mail->Port       = $setting["smtp"]["port"];
        $mail->Username   = $setting["smtp"]["username"];
        $mail->Password   = $setting["smtp"]["password"];

        //send
        if ($mail->send()) {
            return;
        } else {
            echo "Error while sending email: ".$mail->getError();
            return false;
        }
    }

    public static function sendMailWithContent($to,$subject,$content){
        self::sendMail($to,$subject,"empty",array(
            "content" => $content
        ));
    }

    public static function param($name,$default=false)
    {
        if(isset(Yii::app()->params[$name]))
        {
            return Yii::app()->params[$name];
        }
        return $default;
    }

    public static function param2($name,$subName,$default=false)
    {
        $arr = self::param($name);
        if(!$arr)
            return $default;
        return ArrayHelper::get($arr,$subName,$default);
    }
    
    public static function session($name,$default=false)
    {   
        if(isset(Yii::app()->session[$name]))
        {
            return Yii::app()->session[$name];
        }
        return $default;
    }

    public static function setSession($name,$value){
        Yii::app()->session[$name] = $value;
    }

    public static function deleteSession($name){
        if(isset(Yii::app()->session[$name]))
            unset(Yii::app()->session[$name]);
    }

    public static function date($timestamp)
    {
        $dateTime = new DateTime();
        $dateTime->setTimestamp($timestamp);
        return $dateTime;
    }

    public static function log($content,$name="default"){
        $folder = "./logs/";
        if (!file_exists($folder)) {
            mkdir($folder, 0755, true);
        }
        $filename = $folder . $name . ".txt";
        file_put_contents($filename, $content."\n",FILE_APPEND);
    }
}

Util::$uniqueIndex = 0;