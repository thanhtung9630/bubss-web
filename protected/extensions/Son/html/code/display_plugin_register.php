<?php
$sPlugin = Son::load("SPlugin");
$sPlugin->registerDisplayPlugin("link","function",function($value,$config,$htmlAttributes){
	if(ArrayHelper::get($config,"newTab")){
		$htmlAttributes["target"] = "_blank";
	}
	if(!isset($config["content"]))
	{
		if(!$value){
			return;
		}
		echo CHtml::link($value,$value,$htmlAttributes);
		return;
	}
	echo CHtml::link($config["content"],$config["href"],$htmlAttributes);
});

$sPlugin->registerDisplayPlugin("image","function",function($value,$config,$htmlAttributes){
	echo CHtml::image(ArrayHelper::get($config,"url",$value),ArrayHelper::get($config,"alt"),$htmlAttributes);
});

$sPlugin->registerDisplayPlugin("image_thumbnail","file","ext.Son.html.views.display_plugin.image_thumbnail",array(
	//"fancybox"
));

$sPlugin->registerDisplayPlugin("timestamp","function",function($value,$config,$htmlAttributes){
	$htmlAttributes["timestamp"] = $value;
	echo CHtml::tag("span",$htmlAttributes);
});

$sPlugin->registerDisplayPlugin("dropdown_label","function",function($value,$config,$htmlAttributes){
	$attr = $config["attr"];
	$modelClass = $config["modelClass"];
	$label = $modelClass::model()->listGetLabel($attr,$value);
	echo CHtml::tag("span",$htmlAttributes,$label);
},null,null,array(
	"class" => "label label-success"
));

$sPlugin->registerDisplayPlugin("checkbox_label","function",function($value,$config,$htmlAttributes){
	$label = $value ? '<i class="fa fa-check"></i>' : '<i class="fa fa-times"></i>';
	echo CHtml::tag("span",$htmlAttributes,$label);
},null,null,array(
	"class" => "label label-primary"
));

$sPlugin->registerDisplayPlugin("text_short_description","function",function($value,$config,$htmlAttributes){
	$shortValue = $value;
	//$shortValue = TextHelper::getShortDescription($value,ArrayHelper::get($config,"length",100));
	?>
		<div style="text-overflow:ellipsis; overflow-x:hidden"><?php echo $shortValue ?></div>
	<?php
},null,null,array(
	"class" => "label label-success"
));