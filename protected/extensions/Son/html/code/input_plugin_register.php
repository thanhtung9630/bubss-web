<?php
$sPlugin = Son::load("SPlugin");
$sPlugin->registerInputPlugin("timestamp_datepicker","html_attr",array(
	"input-datetime" => "",
	"hidden" => "",
),array(
	"format" => "DD/MM/YYYY"
),array(
	"bootstrap_datetimepicker"
));

$sPlugin->registerInputPlugin("timestamp_datetimepicker","html_attr",array(
	"input-datetime" => "",
	"hidden" => ""
),array(
	"format" => "DD/MM/YYYY HH:mm"
),array(
	"bootstrap_datetimepicker"
));

/*$sPlugin->registerInputPlugin("file_picker","html_attr",array(
	"input-file" => ""
),array(
	"type" => "file"
),array(
	"bootstrap-fileinput"
));*/

$sPlugin->registerInputPlugin("file_picker","function",function($name,$value,$config,$htmlAttributes){
	$htmlAttributes["type"] = "file";
	$htmlAttributes["input-file"] = "";
	$htmlAttributes["data-url"] = $value;
	if($config["multiple"]){
		$htmlAttributes["multiple"] = "multiple";
	}
	echo CHtml::fileField($name,"",$htmlAttributes);
},array(
	"multiple" => false
),array(
	"bootstrap-fileinput"
));


$sPlugin->registerInputPlugin("html","html_attr",array(
	"input-html" => ""
),null,array(
	"summernote"
));

$sPlugin->registerInputPlugin("checkbox_button","html_attr",array(
	"input-checkbox-button" => "",
),array(
	"type" => "checkbox"
),array(
	"icheck"
));

$sPlugin->registerInputPlugin("dropdown","html_attr",array(
	"input-dropdown" => ""
),array(
	"type" => "select"
));

$sPlugin->registerInputPlugin("dropdown_model","function",function($name,$value,$config,$htmlAttributes){
	$attr = $config["attr"];
	$modelClass = $config["modelClass"];
	$data = $modelClass::model()->listDropdownConfig[$attr];
	$htmlAttributes["input-dropdown"] = "";
	echo CHtml::dropdownList($name,$value,$data,$htmlAttributes);
});

$sPlugin->registerInputPlugin("dropdown_model_ajax","function",function($name,$value,$config,$htmlAttributes){
	$data = array();
	$htmlAttributes["data-url"] = ArrayHelper::get($config,"url",function(){
		return Util::controller()->createUrl("search_attr");
	});
	$htmlAttributes["data-value"] = $value;
	if($defaultDisplayAttr=ArrayHelper::get($config,"defaultDisplayAttr")){
		$htmlAttributes["item-display-attr"] = $defaultDisplayAttr;
	}
	if($defaultDisplayValue=ArrayHelper::get($config,"defaultDisplayValue")){
		$htmlAttributes["data-display"] = $defaultDisplayValue;
	}
	$htmlAttributes["data-model"] = strtolower($config["modelClass"]);
	$htmlAttributes["input-dropdown"] = "";
	$htmlAttributes["data-ajax"] = "1";
	$htmlAttributes["data-attr"] = $config["attr"];
	echo CHtml::dropdownList($name,$value,$data,$htmlAttributes);
});