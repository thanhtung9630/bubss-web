<?php
class SForm extends CFormModel {
	var $defaultConfig = array(
		"title" => null,
		"inputs" => array(
			"__item" => array(
				"rules" => false,
				"value" => "",
				"model" => false,
				"displayInput" => true
			)
		),
		"models" => array(),
		"method" => "post",
		"viewParams" => array(
			"submitHtmlAttributes" => array(
				"class" => "btn btn-primary"
			),
		),
		"validate" => true,
		"uploadEnabled" => false,
		"ajaxSubmit" => false,
		"confirm" => false,
		"htmlAttributes" => array(),
		"actionUrl" => "",
		"view" => "ext.Son.html.views.form.form-bootstrap"
	);
	var $config = array();
	var $__data = array();
	public $__error = false;

	public function __get($key){
		if(isset($this->$key))
			return parent::__get($key);
		return ArrayHelper::get($this->__data,$key,"");
	}

	public function __set($key,$value){
		if(isset($this->$key))
			return parent::__set($key,$value);
		$this->__data[$key] = $value;
	}

	public function __construct($config=null){
		parent::__construct();
		if($config){
			$this->config = $config;
		}
		$this->formInit();
	}

	protected function formInit(){
		if($tempConfig=$this->getConfig()){
			$this->config = $tempConfig;
		}
		$this->config = array_replace_recursive($this->defaultConfig,$this->config);
		ArrayHelper::processItemDefault($this->config["inputs"]);
		foreach($this->config["inputs"] as $key => $input) {
			$this->$key = $input["value"];
		}
	}


	public function handle(){
		return $this->onHandleInput();
	}

	public function readInput($multipleError=false){
		$formValid = true;
		foreach($this->config["inputs"] as $inputName => $input){
			if($input["model"])
				continue;
			$hasError = false;
			if($this->config["method"]=="post"){
				list($this->$inputName,$hasError,$firstErrorMessage,$errors) = Input::post($inputName,$input,"return");
			} else {
				list($this->$inputName,$hasError,$firstErrorMessage,$errors) = Input::get($inputName,$input,"return");
			}
			if($hasError){
				$formValid = false;
				foreach($errors as $error){
					$this->addError($inputName,$error);
				}
			}
			if($hasError && $multipleError)
				return false;
		}
		if(!$formValid){
			$this->setError(true);
		}
		return $formValid;
	}

	/*public function readInputToModel(){
		foreach($this->config["models"] as $model => $className){
			$attrs = false;
			if($this->config["method"]=="post"){
				$attrs = Input::post($model,false);
			} else {
				$attrs = Input::get($model,false);
			}
			if(!$attrs)
				continue;
			if($this->$model==null)
				$this->$model = new $className();
			foreach($attrs as $key => $value){
				$this->$model->$key = $value;
			}
			if($this->config["uploadEnabled"]){
				$this->$model->fileGetFromInput($model);
			}
		}
	}*/

	public function readInputToModel($multipleError=false){
		$formValid = true;
		foreach($this->config["models"] as $model => $className){
			if($this->$model==null)
				$this->$model = new $className();
			if($this->config["method"]=="post"){
				$attrs = Input::post($model,array());
			} else {
				$attrs = Input::get($model,array());
			}
			foreach($this->config["inputs"] as $inputName => $input){
				if($input["model"]!=$model)
					continue;
				list($this->$model->$inputName,$hasError,$firstErrorMessage,$errors) = Input::getInput($attrs,$inputName,$input,"return");
				if($hasError){
					foreach($errors as $error){
						$this->$model->addError($inputName,$error);
					}
					$formValid = false;
				}
				if($hasError && $multipleError)
					return false;
			}
			if($this->config["uploadEnabled"]){
				$this->$model->fileGetFromInput($model);
			}
		}
		if(!$formValid){
			$this->setError(true);
		}
		return $formValid;
	}

	public function replaceModelWith($model,$newModel,$attrs=null){
		if($attrs){
			foreach($attrs as $attr){
				if($this->$model->$attr!==null)
					$newModel->$attr = $this->$model->$attr;
			}
		} else {
			foreach($this->config["inputs"] as $inputName => $attr){
				if($this->$model->$attr!==null)
					$newModel->$attr = $this->$model->$attr;
			}
		}
		if($this->config["uploadEnabled"]){
			$newModel->fileList = $this->$model->fileList;
			$newModel->fileConfig = $this->$model->fileConfig;
			$newModel->fileUpdateFileNameAfterSave = $this->$model->fileUpdateFileNameAfterSave;
			foreach($newModel->fileList as $key => $file){
				$file->model = $newModel;
			}
		}
		$this->$model = $newModel;
	}

	public function getInput($inputName){
		return ArrayHelper::get($this->config["inputs"],$inputName);
	}

	// Get Property

	public function inputLabel($inputName){
		$input = $this->getInput($inputName);
		return ArrayHelper::get($input,"label",function() use ($inputName){
			return Yii::t("app",ucfirst($inputName));
		},true);
	}

	public function isError(){
		return $this->__error;
	}

	public function setError($err){
		$this->__error = $err;
	}

	public function inputError($inputName){
		$arr = $this->inputErrors($inputName);
		return ArrayHelper::get($arr,0,"");
	}

	public function inputErrors($inputName){
		$input = $this->config["inputs"][$inputName];
		$modelName = $input["model"];
		if($modelName){
			if(!$this->$modelName)
				return null;
			$arr = $this->$modelName->getErrors();
			return ArrayHelper::get($arr,$inputName,array());
		} else {
			$arr = $this->getErrors();
			return ArrayHelper::get($arr,$inputName,array());
		}
	}

	public function otherError($multiple=false,$separateError=false){
		$error = "";
		if($multiple){
			$error = array();
		}
		foreach($this->config["models"] as $model => $className){
			if(!$this->$model)
				continue;
			$errorList = $this->$model->getErrors();
			foreach($errorList as $inputName => $errors){
				if(!isset($this->config["inputs"][$inputName])){
					// other attribute
					if(!$multiple)
					{
						return $errors[0];
					}
					if($separateError){
						$error[$inputName] = $errors;
					} else {
						$error = array_merge($error,$errors);
					}
					
				}
			}
		}
		if($this->hasErrors()){
			$errorList = $this->getErrors();
			foreach($errorList as $inputName => $errors){
				if(!$multiple)
				{
					return $errors[0];
				}
				if($separateError){
					$error["__form"] = $errors;
				} else {
					$error = array_merge($error,$errors);
				}
			}
		}
		return $error;
	}

	public function getFirstError(){
		return $this->getAllErrors(false);
	}

	public function getAllErrors($multiple=true,$separateError=true){
		$error = "";
		if($multiple){
			$error = array();
		}
		foreach($this->config["models"] as $model => $className){
			if(!$this->$model)
				continue;
			$errorList = $this->$model->getErrors();
			foreach($errorList as $inputName => $errors){
				if(!$multiple)
				{
					return $errors[0];
				}
				if($separateError){
					$error[$inputName] = $errors;
				} else {
					$error = array_merge($error,$errors);
				}
			}
		}
		if($this->hasErrors()){
			$errorList = $this->getErrors();
			foreach($errorList as $inputName => $errors){
				if(!$multiple)
				{
					return $errors[0];
				}
				if($separateError){
					$error["__form"] = $errors;
				} else {
					$error = array_merge($error,$errors);
				}
			}
		}
		return $error;
	}

	public function viewParam($paramName,$default=null){
		return ArrayHelper::get($this->config["viewParams"],$paramName,$default);
	}

	// HTML

	public function open($htmlAttributes=array()){
		$actionUrl = $this->getActionUrl();
		$method = $this->config["method"];
		if($this->config["uploadEnabled"]){
			$method = "post";
			ArrayHelper::set($htmlAttributes,"enctype","multipart/form-data");
		}

		$htmlAttributes = array_merge($this->config["htmlAttributes"],$htmlAttributes);

		if($this->config["confirm"]){
			ArrayHelper::set($htmlAttributes,"data-confirm",$this->config["confirm"]);
		}

		if($this->config["ajaxSubmit"]==false){
			if($this->config["validate"] || $this->config["confirm"]){
				$htmlAttributes["data-type"] = "validate";
				// then change the submit button also
			}
		} else {
			if($this->config["uploadEnabled"]){
				$htmlAttributes["data-type"] = "iframe";
				unset($htmlAttributes["onsubmit"]);
				// then change the submit button also
			} else {
				$htmlAttributes["data-type"] = "ajax";
				$htmlAttributes["onsubmit"] = '$__$.__ajax(this,event)';
			}
		}

		echo CHtml::beginForm($actionUrl,$method,$htmlAttributes);
	}

	public function close(){
		echo CHtml::endForm();
	}

	public function submitButton($content,$htmlAttributes=array()){
		$formUpload = $this->config["ajaxSubmit"] && $this->config["uploadEnabled"];
		$formValidate = !$this->config["ajaxSubmit"] && ($this->config["validate"] || $this->config["confirm"]);
		if($formUpload || $formValidate){
			$htmlAttributes["type"] = "button";
			$htmlAttributes["onclick"] = '$__$.__ajax(this.form,event)';
		} else {
			$htmlAttributes["type"] = "submit";
		}
		echo CHtml::htmlButton($content,$htmlAttributes);
	}

	public function inputField($inputName,$htmlAttributes=array()){
		$input = $this->getInput($inputName);
		if(!$input["displayInput"]){
			return;
		}
		$modelName = $input["model"];
		$arr = ArrayHelper::get($input,"htmlAttributes",array());
		$htmlAttributes = array_merge($arr,$htmlAttributes);
		$value = "";
		if($modelName){
			if($this->$modelName){
				$value = $this->$modelName->$inputName;
			}
			$name = $modelName . "[" . $inputName . "]";
		} else {
			$value = $this->$inputName;
			$name = $inputName;
		}
		Son::load("SPlugin")->renderInput($name,$value,ArrayHelper::get($input,"type"),ArrayHelper::get($input,"config"),$htmlAttributes);
	}

	protected function getView(){
		return $this->config["view"];
	}

	public function render($view=null,$viewParams=null,$return=false){
		if($view==null){
			$view = $this->getView();
		}
		if($viewParams){
			$this->config["viewParams"] = array_replace_recursive($this->config["viewParams"], $viewParams);
		}
		return Util::controller()->renderPartial($view,array(
			"form" => $this
		),$return);
	}

	public function renderPage($view=null,$viewParams=null,$return=false){
		if($view==null){
			$view = $this->getView();
		}
		if($viewParams){
			$this->config["viewParams"] = array_replace_recursive($this->config["viewParams"], $viewParams);
		}
		return Util::controller()->render($view,array(
			"form" => $this
		),$return);
	}

	public function returnJson($successData=null,$includeAllErrors=false){
		if($this->isError()){
			Output::returnJsonError($this->getFirstError(),200,1,$includeAllErrors ? $this->getAllErrors() : null);
		} else {
			Output::returnJsonSuccess($successData);
		}
	}

	public function setCurrentUrl(){
		$this->config["actionUrl"] = Util::controller()->createUrl();
	}

	protected function getActionUrl(){
		return $this->config["actionUrl"];
	}

	protected function getConfig(){
		return null;
	}

	protected function onHandleInput(){
		if($onHandleInput = ArrayHelper::get($this->config,"onHandleInput")){
			return $onHandleInput($this);
		}
		return false;
	}
}