<?php
$adminTableHelper = Son::load("AdminTableHelper");

$adminTableHelper->registerActionButton("delete","function",function($item,$config,$htmlAttributes){
	$htmlAttributes["item-action-delete"] = "";
	if($message = $config["message"]){
		$htmlAttributes["item-message"] = $message;
	}
	if($title=$config["title"]){
		$htmlAttributes["data-toggle"] = "tooltip";
		$htmlAttributes["title"] = $title;
	}
	echo CHtml::htmlButton($config["content"],$htmlAttributes);
},array(
	"content" => '<i class="fa fa-trash"></i>',
	"message" => "Are you sure to delete this item? This cannot be undone!",
	"title" => "Delete this item"
),array(
	"class" => $adminTableHelper->defaultButtonClass,
));

$adminTableHelper->registerActionButton("update","function",function($item,$config,$htmlAttributes){
	$htmlAttributes["item-action-update"] = "";
	if($title=$config["title"]){
		$htmlAttributes["data-toggle"] = "tooltip";
		$htmlAttributes["title"] = $title;
	}
	echo CHtml::htmlButton($config["content"],$htmlAttributes);
},array(
	"content" => '<i class="fa fa-edit"></i>',
	"title" => "Edit"
),array(
	"class" => $adminTableHelper->defaultButtonClass,
));

$adminTableHelper->registerActionButton("extended_action","function",function($item,$config,$htmlAttributes){
	$htmlAttributes["item-extended-action"] = $config["action"];
	if($message = $config["message"]){
		$htmlAttributes["item-message"] = $message;
	}
	if($title=$config["title"]){
		$htmlAttributes["data-toggle"] = "tooltip";
		$htmlAttributes["title"] = $title;
	}
	echo CHtml::htmlButton($config["content"],$htmlAttributes);
},array(
	"message" => false,
	"title" => false
),array(
	"class" => "btn btn-sm btn-alert"
));

$adminTableHelper->registerActionButton("detail","file","ext.Son.super-modules.admin-table.views.action-button.detail",array(
	"content" => '<i class="fa fa-info"></i>',
	"header" => false,
	"headerCloseButton" => false,
	"closeButton" => "Close"
),array(
	"class" => $adminTableHelper->defaultButtonClass,
	"title" => "Detail",
	"data-toggle" => "tooltip"
));

$adminTableHelper->registerActionButton("link","function",function($item,$config,$htmlAttributes){
	if($config["href"]===false)
		return;
	if($title=$config["title"]){
		$htmlAttributes["data-toggle"] = "tooltip";
		$htmlAttributes["title"] = $title;
	}
	if($config["newTab"]){
		$htmlAttributes["target"] = "_blank";
	}
	echo CHtml::link($config["content"],$config["href"],$htmlAttributes);
},array(
	"content" => '<i class="fa fa-external-link"></i>',
	"href" => "javascript:;",
	"title" => false,
	"newTab" => true
),array(
	"class" => "btn btn-sm btn-success"
));

$adminTableHelper->registerActionButton("content_display","file","ext.Son.super-modules.admin-table.views.action-button.content_display",array(
	"content" => '<i class="fa fa-search-plus"></i>',
	"header" => false,
	"headerCloseButton" => false,
	"closeButton" => false
),array(
	"class" =>  $adminTableHelper->defaultButtonClass
));