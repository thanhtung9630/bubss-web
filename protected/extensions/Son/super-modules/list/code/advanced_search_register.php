<?php
$sListHelper = Son::load("SListHelper");
$sListHelper->advancedSearchRegisterType("timestamp_range_datetimepicker","file","ext.Son.super-modules.list.views.advanced-search.timestamp_range_datetimepicker","range",array(
	"placeholder_1" => "From",
	"placeholder_2" => "To"
));

$sListHelper->advancedSearchRegisterType("text_match_partial","function",function($name,$value,$config,$htmlAttributes){
	echo CHtml::textField($name,$value,$htmlAttributes);
},"match_partial",array(
	"triggerType" => "enter"
));