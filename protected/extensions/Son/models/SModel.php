<?php
class SModel extends GxActiveRecord {

	/* FILE CONSTATNS */

	const SCENARIO_UPDATE_FILE_NAME = "SCENARIO_UPDATE_FILE_NAME";

	/* CACHE CONSTANTS */

	const CACHE_FILE_DEPENDENCY_FOLDER = "_cache_file_dependency";

	/* DYNAMIC PROPERTIES VARIABLES */

	protected $dynamicPropertyValues = array();

	/* MODEL VARIABLES */

	protected $deactiveInsteadOfDelete = false;
	protected $autoUpdateUpdatedTime = "updated_time";
	protected $autoUpdateCreatedTime = "created_time";
	public $listDropdownConfig = null;
	protected $defaultValues = array();

	public $modelUpdateTimeFlag = false;

	/* PASSWORD VARIABLES */
	protected $passwordField = null;
	protected $passwordBackup = "";
	protected $passwordRaw = "";
	protected $passwordChangedFlag = false;
	protected $passwordHolder = "-----------------";

	/* FILE UPLOAD VARIABLES */

	/**
		* $fileConfig is an array like array(avatar => array( "type" => "image", "extension" => "png", "size" => array(1,100), "image" => array("fixSize" => array(), "minSize" => array(), "maxSize" => array(), "resize" => array()), "folder" => function(){}, "fileName" => function(){}, "thumbnail" => array("thumbnail") ), "thumbnail" => array( "image" => array( "resize" => array()) ))
	*/
	protected $fileUploadEnabled = false;
	protected $fileClassName = "ModelFile";
	protected $fileOldPath = array();
	public $fileUpdateFileNameAfterSave = false;
	public $fileConfig = array();
	public $fileList = array();

	/* CACHE VARIABLES */
	protected $cacheEnabled = false;
	protected $cacheDependencyType = "file";
	protected $cacheDuration = 2592000; // One month
	protected $cacheName = null;
	protected $cacheChangedColumn = "max(updated_date)";
	protected $cacheDependencyId = null;
	protected $cacheInvalidateIds = null;

	/* CONSTRUCTOR */

	/* DYNAMIC PROPERTIES VARIABLE */

	public function dynamicPropertyIsSet($property){
		return ArrayHelper::get($this->dynamicPropertyValues,$property);
	}

	public function __isset($key){
		return parent::__isset($key) || Son::load("SModelHelper")->hasProperty(get_class($this),$key);
	}

	public function __get($key){
		if(Son::load("SModelHelper")->hasProperty(get_class($this),$key)){
			return ArrayHelper::get($this->dynamicPropertyValues,$key);
		}
		return parent::__get($key);
	}

	public function __set($key,$value){
		if(Son::load("SModelHelper")->hasProperty(get_class($this),$key)){
			$this->dynamicPropertyValues[$key] = $value;
			return;
		}
		parent::__set($key,$value);
	}

	/* MODEL FUNCTIONS */


	protected function getExtendedRules(){
		return null;
	}

	protected function initializeDefaultValues(){
		foreach ($this->defaultValues as $key => $value) {
			$this->$key = $value;
		}
	}

	protected function autoUpdateTimeFunction(){
		return time();
	}

	private function updateTime(){
		if($this->modelUpdateTimeFlag)
			return;
		if($autoUpdateCreatedTime = $this->autoUpdateCreatedTime){
			if($this->getIsNewRecord()){
				$this->$autoUpdateCreatedTime = $this->autoUpdateTimeFunction();
			}
		}
		if($autoUpdateUpdatedTime = $this->autoUpdateUpdatedTime){
			$this->$autoUpdateUpdatedTime = $this->autoUpdateTimeFunction();
		}
		$this->modelUpdateTimeFlag = true;
	}

	public function getFirstError(){
        foreach($this->getErrors() as $errorsOfAttr){
            return $errorsOfAttr[0];
        }
        return null;
    }

    public function listSearch($searchAttr,$searchTerm){
    	$searchConfig = ArrayHelper::get($this->listDropdownConfig,$searchAttr);
    	$criteria = new CDbCriteria;
    	foreach($searchConfig["searchAttrs"] as $attr){
    		$criteria->compare($attr,$searchTerm,true,"or",true);
    	}
    	$model = $searchConfig["model"];
		$items = $model::model()->findAll($criteria);
		$arr = array();
		$valueAttr = $searchConfig["valueAttr"];
		$displayAttr = $searchConfig["displayAttr"];
		foreach($items as $item){
			$arr[] = array($item->$valueAttr,$item->$displayAttr);
		}
		return $arr;
    }

	public function listGetLabel($attr,$value=null){
		if($value===null)
			$value = $this->$attr;
		return $this->listDropdownConfig[$attr][$value];
	}

	/* SECURITY FUNCTIONS */

	protected function securityToBeXssCleanProperties(){
		return null;
	}

	protected function securityCleanXss(){
		$properties = $this->securityToBeXssCleanProperties();
		if(!$properties)
			return;
		foreach ($properties as $property) {
			$this->$property = htmlspecialchars($this->$property,ENT_QUOTES,'UTF-8');
		}
	}
	
    /* PASSWORD FUNCTIONS */

    public function verifyPassword($password)
    {
        return CPasswordHelper::verifyPassword($password,$this->passwordBackup);
    }
 
    protected function hashPassword($password)
    {
        return CPasswordHelper::hashPassword($password);
    }

    protected function passwordDoAfterFind(){
    	if(!$this->passwordField)
    		return;
    	$passwordField = $this->passwordField;
    	$this->passwordBackup = $this->$passwordField;
    	$this->$passwordField = null;
    	$this->passwordChangedFlag = false;
    }

    protected function passwordDoBeforeValidate(){
    	if(!$this->passwordField)
    		return;
    	$passwordField = $this->passwordField;
    	if($this->$passwordField){
    		// password has changed => set passwordChangedFlag
    		$this->passwordChangedFlag = true;
    	} else {
    		// password has not changed => set password to passwordHolder to have a valid value and set passwordChangedFlag
    		$this->passwordChangedFlag = false;
    		$this->$passwordField = $this->passwordHolder;
    	}
    }

    protected function passwordDoAfterValidate(){
    	if(!$this->passwordField)
    		return;
    	$passwordField = $this->passwordField;
    	if($this->hasErrors()){
    		// has error => revert backup
    		if(!$this->passwordChangedFlag){
    			// the current password id passwordHolder
    			$this->$passwordField = null;
    		}

    	}
    }

    protected function passwordDoBeforeSave(){
    	if(!$this->passwordField)
    		return;
    	$passwordField = $this->passwordField;
    	if($this->passwordChangedFlag){
    		// password has changed => hash
    		$this->passwordRaw = $this->$passwordField;
    		$this->$passwordField = $this->hashPassword($this->$passwordField);
    	} else {
    		// password has not changed => restore password
    		$this->$passwordField = $this->passwordBackup;
    	}
    }

    protected function passwordDoAfterSave(){
    	if(!$this->passwordField)
    		return;
    	$passwordField = $this->passwordField;
    	if($this->hasErrors()){
    		// error => revert
    		if($this->passwordChangedFlag){
    			$this->$passwordField = $this->passwordRaw;
    		} else {
    			$this->$passwordField = null;
    		}
    	}
    }

    /* FILE UPLOAD FUNCTIONS */

    public function fileAttrIsFile($attr){
    	return isset($this->fileConfig[$attr]);
    }

    protected function getFileConfig(){
    	return null;
    }

    protected function fileGetFolderToBeDeleted(){
    	return null;
    }

    protected function fileInitializeDefaultValues(){
    	if(!$this->fileUploadEnabled)
    		return;
    	foreach($this->fileConfig as $key => $config){
    		$val = ArrayHelper::get($config,"defaultUrl");
    		if(!$this->$key && $val){
    			$this->$key = $val;
    		}
    	}
    }

    /**
    	* @param $wrapper can be null or a string, with the input like User[avatar], the wrapper will be User
    */
    public function fileGetFromInput($wrapper=null){
    	if(!$this->fileUploadEnabled)
    		return;
    	$className = $this->fileClassName;
    	//$this->fileProcessFileConfig();
    	if($wrapper==null){
    		foreach($_FILES as $key => $fileArr){
    			if(!$fileArr["name"])
    				continue;
    			if(!($config = ArrayHelper::get($this->fileConfig,$key)))
    				continue;
    			if($fileArr["error"]==UPLOAD_ERR_NO_FILE){
	        		continue;
	        	}
	            $file = new $className();
	            $file->key = $key;
	            $file->name = $fileArr["name"];
	            $file->type = $fileArr["type"];
	            $file->tmp_name = $fileArr["tmp_name"];
	            $file->size = filesize($file->tmp_name);
	            $file->error = $fileArr["error"];
	            $file->model = $this;
	            $this->fileList[$key] = $file;

	            if($thumbnails = ArrayHelper::get($config,"thumbnails")){
	            	foreach($thumbnails as $thumbnail){
	            		$thumbnailFile = new $className();
	            		$thumbnailFile->name = $file->name;
			            $thumbnailFile->tmp_name = $file->tmp_name;
			            $thumbnailFile->type = $file->type;
			            $thumbnailFile->key = $thumbnail;
			            $thumbnailFile->model = $this;
			            $this->fileList[$thumbnail] = $thumbnailFile;
			            $this->fileConfig[$thumbnail]["src"] = $file->key;
	            	}
	            }

	        }
    	} else {
    		$modelFiles = ArrayHelper::get($_FILES,$wrapper);
    		if(!$modelFiles)
    			return;
	        foreach($modelFiles["name"] as $key => $fileName){
	        	if(!$fileName)
	        		continue;
	        	if(!($config = ArrayHelper::get($this->fileConfig,$key)))
	        		continue;
	        	if($modelFiles["error"][$key]==UPLOAD_ERR_NO_FILE){
	        		continue;
	        	}
	        	//print_r($this->fileConfig);die();
	            $file = new $className();
	            $file->key = $key;
	            $file->name = $modelFiles["name"][$key];
	            $file->type = $modelFiles["type"][$key];
	            $file->tmp_name = $modelFiles["tmp_name"][$key];
	            $file->size = filesize($file->tmp_name);
	            $file->error = $modelFiles["error"][$key];
	            $file->model = $this;
	        	$this->fileList[$key] = $file;

	        	if($thumbnails = ArrayHelper::get($config,"thumbnails")){
	            	foreach($thumbnails as $thumbnail){
	            		$thumbnailFile = new $className();
	            		$thumbnailFile->name = $file->name;
			            $thumbnailFile->tmp_name = $file->tmp_name;
			            $thumbnailFile->type = $file->type;
			            $thumbnailFile->key = $thumbnail;
			            $thumbnailFile->model = $this;
			            $this->fileList[$thumbnail] = $thumbnailFile;
			            $this->fileConfig[$thumbnail]["src"] = $file->key;
	            	}
	            }
	        }
    	}
    	foreach($this->fileList as $key => $file){
    		if($config = ArrayHelper::get($this->fileConfig,$key)){
    			$updateFileNameAfterSave = ArrayHelper::get($config,"updateFileNameAfterSave",false);
    			if($updateFileNameAfterSave){
    				if(!is_array($this->fileUpdateFileNameAfterSave)){
    					$this->fileUpdateFileNameAfterSave = array();
    				}
    				$this->fileUpdateFileNameAfterSave[] = $key;
    			}
    		}
    	}
    }

    protected function fileValidate(){
    	if(!$this->fileUploadEnabled)
    		return true;
    	//print_r($this->fileList);die();
    	foreach($this->fileList as $key => $file){
    		if($file->error){
    			$labels = $this->attributeLabels();
    			$this->addError($key, $labels[$key] . " upload error. Please choose another file");
    			return false;
    		}
    		$config = $this->fileConfig[$key];
    		$file->setConfig($config);
    		$this->fileOldPath[$key] = $this->$key;
    		$result = $file->validateWithConfig();
    		if(!$result){
    			return false;
    		}
    	}
    	return true;
    }

    /**
    	* Replace the old filePath with the new one
    	* This function will be called after all the files were validated and $fileList, $fileOldPath is available
    */
    protected function fileSaveNew(){
    	if(!$this->fileUploadEnabled)
    		return;
    	foreach($this->fileList as $key => $file){
    		$config = $this->fileConfig[$key];
    		if(ArrayHelper::get($config,"src")) // this kind of file will be saved in it parent
    			continue;
    		$result = $file->saveWithConfig();
    		if(!$result)
    			return;
    	}
    }

    protected function fileRemoveOld(){
    	if(!$this->fileUploadEnabled)
    		return;
    	foreach($this->fileOldPath as $key => $oldPath){
    		if($oldPath && ($oldPath != $this->$key)){ // this attr is not empty and has been changed
    			$config = $this->fileConfig[$key];
    			if(ArrayHelper::get($config,"defaultUrl")==$oldPath)
    				continue;
    			$result = @unlink(substr($oldPath,1));
    			//$result = unlink($oldPath);
    		}
    	}
    }

    /**
    	* Remove all path that is contained in the record
    */
    protected function fileRemoveAll(){
    	if(!$this->fileUploadEnabled)
    		return;
    	//$this->fileProcessFileConfig();
    	foreach($this->fileConfig as $key => $file){
    		$filePath = $this->$key;
    		if(!$filePath)
    			continue;
    		$config = $this->fileConfig[$key];
			if(ArrayHelper::get($config,"defaultUrl")==$filePath)
				continue;
    		$result = @unlink(substr($filePath, 1));
    	}
    	$folderToBeDeleted = $this->fileGetFolderToBeDeleted();
    	if(!$folderToBeDeleted)
    		return;
    	Util::deleteFile($folderToBeDeleted);
    }


    protected function fileProcessFileConfig(){
    	if(!$this->fileUploadEnabled)
    		return;
    	$fileConfigFromFunction = $this->getFileConfig();
    	if($fileConfigFromFunction){
    		$this->fileConfig = $fileConfigFromFunction;
    	}
    }

	/* CACHE FUNCTIONS */

	/**
		* Used when query from database
		* @param $dependencyId the id which the requested query will depend on, this can be an array of key value pair or an sql where statement like shop_id = 1
	*/
	protected function cacheEncodeArrayToId($arr){
		$str = "";
		foreach($arr as $key => $value){
			$str .= $key . "_" . $value . "_";
		}
		return $str;
	}

	public function cacheSetDependencyId($dependencyId="global"){
		if(is_array($dependencyId))
			$dependencyId = $this->cacheEncodeArrayToId($dependencyId);
		$this->cacheDependencyId = $dependencyId;
	}

	/**
		* Used when update a record from database
		* @param $invalidateIds the id (a string or an array) of cache which will be invalidated
	*/
	public function cacheSetInvalidateId($invalidateIds){
		if(!isset($invalidateIds[0])) // is not a sequence array
			$invalidateIds = array($invalidateIds);
		foreach($invalidateIds as $i => $invalidateId){
			if(is_array($invalidateId)){
				$invalidateIds[$i] = $this->cacheEncodeArrayToId($invalidateId);
			}
		}
		$this->cacheInvalidateIds = $invalidateIds;
	}

	public function cacheGetUniqueOfObject(){
		return ArrayHelper::get($cacheConfig,"name",get_class($this));
	}
	
	public function cacheGetFullDependencyId(){
		return $this->cacheGetUniqueOfObject() . "/" . $this->dependencyId();
	}

	public function cacheGetFullInvalidateIds(){
		$arr = array();
		$name = $this->cacheGetUniqueOfObject();
		foreach ($this->invalidateIds as $invalidateId) {
			$arr[] = $name . "/" . $invalidateId;
		}
		return $arr;
	}

	public function cacheGetFilePath($fileName){
		$fileName = self::CACHE_FILE_DEPENDENCY_FOLDER . "/" . $fileName;
		return $fileName;
	}

	public function cacheApplyDependency(){
		if(!$this->cacheEnabled)
			return;
		$dependency = null;
		switch($this->cacheDependencyType){
			case "file":
				$fullDependencyId = $this->cacheGetFullDependencyId();
				$fileName = $this->cacheGetFilePath($fullDependencyId);
				$dependency = new CFileCacheDependency($fileName);
				break;
			case "db":
				$changedColumn = $this->changedColumn;
				$table = $this->tableName();
				$where = "";
				if($this->cacheDependencyId!=null){
					$where = " where " . $this->cacheDependencyId;
				}
				$dependency = new CDbCacheDependency("select $changedColumn from $table $where");
				break;
		}
		if($dependency==null)
			return;
		$this->cache($this->cacheDuration,$dependency);
	}

	public function cacheInvalidate(){
		if(!$this->cacheEnabled || $this->cacheInvalidateIds==null)
			return;
		$fullInvalidateIds = $this->cacheGetFullInvalidateIds();
		switch($this->cacheDependencyType){
			case "file":
				$now = time();
				foreach($fullInvalidateIds as $fullInvalidateId){
					$fileName = $this->cacheGetFilePath($fullInvalidateId);
					file_put_contents($fileName, $now);
				}
				break;
			case "db":
				// do nothing
				break;
		}
	}

	/* APPLY TO OVERRIDE FUNCTIONS */

	public function init(){
		parent::init();
		$this->fileProcessFileConfig();
		$this->initializeDefaultValues();
	}

	public function findAll($condition='', $params=array()){
		$this->cacheApplyDependency();
		return parent::findAll($condition,$params);
	}

	public function findAllByAttributes($attributes,$condition='',$params=array())
	{
		$this->cacheApplyDependency();
		return parent::findAllByAttributes($attributes,$condition,$params);
	}

	public function findAllBySql($sql,$params=array())
	{
		$this->cacheApplyDependency();
		return parent::findAllBySql($sql,$params);
	}

	public function findByAttributes($attributes,$condition='',$params=array())
	{
		$this->cacheApplyDependency();
		return parent::findByAttributes($attributes,$condition,$params);
	}

	public function findBySql($sql,$params=array()){
		$this->cacheApplyDependency();
		return parent::findBySql($sql,$params);
	}

	public function findByPk($pk,$condition='', $params=array()){
		if($this->cacheDependencyId==null){
			$arr = array();
			$arr[$this->primaryKey()] = $pk;
			$this->cacheSetDependencyId($arr);
		}
		$this->cacheApplyDependency();
		return parent::findByPk($pk,$condition,$params);
	}
	
	public function validate($attributes=null, $clearErrors=true){
		$result = parent::validate($attributes,$clearErrors);
		$this->passwordDoAfterValidate();
		return $result;
	}

	public function save($runValidation=true, $attributes=null){
		if($attributes!=null){
			$this->updateTime();
			if($autoUpdateCreatedTime = $this->autoUpdateCreatedTime){
				$attributes[] = $autoUpdateCreatedTime;
			}
			if($autoUpdateUpdatedTime = $this->autoUpdateUpdatedTime){
				$attributes[] = $autoUpdateUpdatedTime;
			}
		}
		$result = parent::save($runValidation,$attributes);
		$this->passwordDoAfterSave();
		if($result)
		{
			$this->cacheInvalidate();
		}
		return $result;
	}

	public function delete(){
		if($activeAttr = $this->deactiveInsteadOfDelete){
			$this->$activeAttr = 0;
			$result = $this->save(false,array(
				$activeAttr
			));
		} else {
			$result = parent::delete();
			if($result)
			{
				$this->cacheInvalidate();
			}
		}
		
		return $result;
	}

	/**
		* Be careful, updateAll method will not affect files
	*/
	public function updateAll($attributes, $condition='', $params=array()){
		if($autoUpdateUpdatedTime = $this->autoUpdateUpdatedTime){
			$attributes[$autoUpdateUpdatedTime] = $this->autoUpdateTimeFunction();
		}
		$result = parent::updateAll($attributes,$condition,$params);
		if($result)
		{
			$this->cacheInvalidate();
		}
		return $result;
	}

	/**
		* Be careful, deleteAll method will not affect files
	*/
	public function deleteAll($condition='', $params=array()){
		$result;
		if($activeAttr = $this->deactiveInsteadOfDelete){
			$arr = array();
			$arr[$activeAttr] = 0;
			$result = $this->updateAll($arr,$condition,$params);
		} else {
			$result = parent::deleteAll($condition,$params);
		}
		if($result)
		{
			$this->cacheInvalidate();
		}
		return $result;
	}

	protected function beforeValidate(){
		if(!parent::beforeValidate())
			return false;
		if(!$this->fileValidate())
			return false;
		$this->passwordDoBeforeValidate();
		$this->updateTime();
		return true;
	}

	protected function beforeSave(){
		if(!parent::beforeSave())
			return false;
		if($this->scenario==self::SCENARIO_UPDATE_FILE_NAME){
			return true;
		}
		$this->passwordDoBeforeSave();
		if(!$this->fileUpdateFileNameAfterSave){
			// update file name before save
			$this->fileSaveNew();
		}
		return true;	
	}

	protected function afterSave(){
		if($this->scenario==self::SCENARIO_UPDATE_FILE_NAME){
			return;
		}
		if($this->fileUpdateFileNameAfterSave){
			$this->fileSaveNew();
			$previousScenario = $this->scenario;
			$this->scenario = self::SCENARIO_UPDATE_FILE_NAME;
			$this->setIsNewRecord(false);
			$this->save(false,$this->fileUpdateFileNameAfterSave);
			$this->scenario = $previousScenario;
		}
		$this->fileRemoveOld();
		parent::afterSave();
	}

	protected function afterDelete(){
		$this->fileRemoveAll();
		return parent::afterDelete();
	}

	protected function afterFind(){
		parent::afterFind();
		$this->passwordDoAfterFind();
		$this->fileInitializeDefaultValues();
		$this->securityCleanXss();
	}

	public function newRules(){
		$arr = parent::rules();
		if($extendedRules = $this->getExtendedRules()){
			foreach($extendedRules as $extendedRule){
				$arr[] = $extendedRule;
			}
		}
		return $arr;
	}

	public function url($attr){
		$val = $this->$attr;
		if(!$val){
			$val = ArrayHelper::get($this->fileConfig[$attr],"defaultUrl");
			//var_dump($this->fileConfig); die();
		}
		return Util::controller()->createAbsoluteUrl($val);
	}
}