<?php
class SController extends CController {
	public $data = array();
	var $pageTitle = "";
	var $defaultPluginLoaded = false;

	public function render($view,$data=null,$return=false){
		// asset
		Son::load("SAsset")->run();
		if($data===null){
			$data = $this->data;
		}
		return parent::render($view,$data,$return);
	}

	public function renderPartial($view,$data=null,$return=false,$processOutput=false){
		if(!$this->defaultPluginLoaded){
			$this->defaultPluginLoaded = true;
			Son::loadFile("ext.Son.html.code.display_plugin_register");
			Son::loadFile("ext.Son.html.code.input_plugin_register");
		}
		return parent::renderPartial($view,$data,$return,$processOutput);
	}

	public function getThemePath($view){
		$themeName = Yii::app()->theme->name;
		return "webroot.themes.$themeName.views.$view";
	}

	public function renderTheme($view,$data=null,$return=false){
		return $this->render($this->getThemePath($view),$data,$return);
	}

	public function renderPartialTheme($view,$data=null,$return=false,$processOutput=false){
		return $this->renderPartial($this->getThemePath($view),$data,$return,$processOutput);
	}

	public function actionSearch_attr(){
		$modelClass = Input::get("model",array(
			"rules" => array(
				array("required"),
				array("length","allowEmpty" => false, "min" => 1)
			)
		),"json");
		$searchTerm = Input::get("term","","json");
		$searchAttr = Input::get("attr",array(
			"rules" => array(
				array("required"),
				array("length","allowEmpty" => false, "min" => 1)
			)
		),"json");
		try {
			$modelClass = ucfirst($modelClass);
			$result = $modelClass::model()->listSearch($searchAttr,$searchTerm);
		} catch(Exception $e){
			Output::returnJsonError("Invalid request");
			return;
		}
		if($result===false)
		{
			Output::returnJsonError("Invalid request");
			return;
		}
		Output::returnJsonSuccess($result);
	}
}