<?php
$path = dirname(__FILE__) . "/mpdf.php";
include_once($path);
$m = new Mpdf();
$url = Yii::app()->createAbsoluteUrl("/");
$m->useSubstitutions = true; // optional - just as an example
$m->SetHeader(Util::param("PDF_INVOICE_HEADER"));  // optional - just as an example
$m->CSSselectMedia='mpdf'; // assuming you used this in the document header
$m->setBasePath($url);

return $m;