phpDownloadsDocumentationGet InvolvedHelp

Search
PHP 5.6.14 is available
mcrypt_cfb � � Mcrypt Functions
PHP Manual Function Reference Cryptography Extensions Mcrypt Mcrypt Functions
Change language:  
Edit Report a Bug
mcrypt_cbc

(PHP 4, PHP 5)
mcrypt_cbc � Encrypts/decrypts data in CBC mode

Description �

string mcrypt_cbc ( int $cipher , string $key , string $data , int $mode [, string $iv ] )
string mcrypt_cbc ( string $cipher , string $key , string $data , int $mode [, string $iv ] )
The first prototype is when linked against libmcrypt 2.2.x, the second when linked against libmcrypt 2.4.x or higher. The mode should be either MCRYPT_ENCRYPT or MCRYPT_DECRYPT.

This function should not be used anymore, see mcrypt_generic() and mdecrypt_generic() for replacements.

Warning
This function was DEPRECATED in PHP 5.5.0, and REMOVED as of PHP 7.0.0.
add a note add a note
User Contributed Notes 3 notes

up
down
4 Bubba at hines57 dot com �3 years ago
The PERL libraries have changed a little bit and getting PHP and PERL to mcrypt together can be confusing, so here is a current example; 

PHP 
=== 
<?php 
$string = 'Some Secret thing I want to encrypt'; 
$iv = '12345678'; 
$passphrase = '8chrsLng'; 

$encryptedString = encryptString($string, $passphrase, $iv); 
// Expect: 7DjnpOXG+FrUaOuc8x6vyrkk3atSiAf425ly5KpG7lOYgwouw2UATw== 

function encryptString($unencryptedText, $passphrase, $iv) { 
  $enc = mcrypt_encrypt(MCRYPT_BLOWFISH, $passphrase, $unencryptedText, MCRYPT_MODE_CBC, $iv); 
  return base64_encode($enc); 
} 
?> 

PERL 
==== 
$encryptedString = '7DjnpOXG+FrUaOuc8x6vyrkk3atSiAf425ly5KpG7lOYgwouw2UATw=='; 
$iv = '12345678'; 
$passphrase = '8chrsLng'; 

$string = &decryptPhpEncrypted $encryptedString, $passphrase, $iv; 
# Expect: Some Secret thing I want to encrypt 

sub decryptPhpEncrypted() { 
  my ($encryptedString, $passphrase, $iv) = @_; 
  my $keysize = length($passphrase); 
  use Crypt::CBC; 
  $cipher = Crypt::CBC->new( {'key' => $encryptedString, 
                              'cipher'=> 'Blowfish', 
                              'iv' => $iv, 
                              'keysize' => $keysize, 
                              'regenerate_key' => 0, 
                              'padding' => 'null', 
                              'prepend_iv' => 0}); 

  return $cipher->decrypt($encryptedString); 
}
up
down
-3 Othman �6 years ago
The following code is to 
encrypt-->encode 
decode-->decrypt

<?php
$stuff="String to enc/enc/dec/dec =,=,";
$key="XiTo74dOO09N48YeUmuvbL0E";

function nl() {
    echo "<br/> \n";
}
$iv = mcrypt_create_iv (mcrypt_get_block_size (MCRYPT_TripleDES, MCRYPT_MODE_CBC), MCRYPT_DEV_RANDOM);

// Encrypting
function encrypt($string, $key) {
    $enc = "";
    global $iv;
    $enc=mcrypt_cbc (MCRYPT_TripleDES, $key, $string, MCRYPT_ENCRYPT, $iv);

  return base64_encode($enc);
}

// Decrypting 
function decrypt($string, $key) {
    $dec = "";
    $string = trim(base64_decode($string));
    global $iv;
    $dec = mcrypt_cbc (MCRYPT_TripleDES, $key, $string, MCRYPT_DECRYPT, $iv);
  return $dec;
}

$encrypted = encrypt($stuff, $key);
$decrypted = decrypt($encrypted, $key);

echo "Encrypted is ".$encrypted . nl(); 
echo "Decrypted is ".$decrypted . nl(); 
?>

Notes on the result 
-o running the script from the command line 1 time 
    php script.php works correct
-o if i ran the script in a loop i.e.

#!/bin/bash
for x in `seq 100` 
do
        echo $x
        php script.php >> LOG.text 
        sleep 1

done
it gets slower after the 10th time
+ inconsistent output 
sometimes correct some with encrypted characters at the end of the decrypted string 

-o Firefox on Linux it decrypts to the original string but appends 
    some encryption at the end of the decrypted sting
-o running the script on the browser from windows on Firefox, 
    Google Chrome, IE7 works fine for just few times 
    "refresh every few seconds" 
    if refresh fast it doesn't work correctly, 
    some times returns the decrypted as encrypted!
    some times returns mix of encrypted & decrypted

I thought if sharing that test for those functions might be 
useful for some body
up
down
-3 dafuk at dafuk dot co dot il �3 years ago
if you use blowfish and somehow when you compare strings before and after it was  (usually when it's shorter than 8 or 16 bytes) you might notice the difference, it comes out that this function does not remove padding, as a result you get a bunch of nulls at the end.

use
$decrypted = rtrim($decrypted,"\0"); 
to fix it
add a note add a note
Mcrypt Functions
mcrypt_?cbc
mcrypt_?cfb
mcrypt_?create_?iv
mcrypt_?decrypt
mcrypt_?ecb
mcrypt_?enc_?get_?algorithms_?name
mcrypt_?enc_?get_?block_?size
mcrypt_?enc_?get_?iv_?size
mcrypt_?enc_?get_?key_?size
mcrypt_?enc_?get_?modes_?name
mcrypt_?enc_?get_?supported_?key_?sizes
mcrypt_?enc_?is_?block_?algorithm_?mode
mcrypt_?enc_?is_?block_?algorithm
mcrypt_?enc_?is_?block_?mode
mcrypt_?enc_?self_?test
mcrypt_?encrypt
mcrypt_?generic_?deinit
mcrypt_?generic_?end
mcrypt_?generic_?init
mcrypt_?generic
mcrypt_?get_?block_?size
mcrypt_?get_?cipher_?name
mcrypt_?get_?iv_?size
mcrypt_?get_?key_?size
mcrypt_?list_?algorithms
mcrypt_?list_?modes
mcrypt_?module_?close
mcrypt_?module_?get_?algo_?block_?size
mcrypt_?module_?get_?algo_?key_?size
mcrypt_?module_?get_?supported_?key_?sizes
mcrypt_?module_?is_?block_?algorithm_?mode
mcrypt_?module_?is_?block_?algorithm
mcrypt_?module_?is_?block_?mode
mcrypt_?module_?open
mcrypt_?module_?self_?test
mcrypt_?ofb
mdecrypt_?generic
Copyright � 2001-2015 The PHP Group My PHP.net Contact Other PHP.net sites Mirror sites Privacy policy
To Top